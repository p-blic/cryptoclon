TARGET_FILE = "CryptoClon.sh"
TARGET_CONF = "coin.conf"
PRJ_CAP = "${PWD}/script/inici.sh"
PRJ_FNC = $(shell ls -d ${PWD}/funcions/*)
PRJ_MOD = $(shell ls -d ${PWD}/moduls/*)
PRJ_MNU = "${PWD}/script/menu.sh"
PRJ_CNF = "${PWD}/script/coin.conf.example"

export PRJ_CAB
export PRJ_FNC
export PRJ_MOD
export PRJ_MNU

SHELL := /usr/bin/env bash

all:		define_main	add_funcions	add_moduls	add_menu	add_conf_bitcoin	set_perms

mycoin:	define_main	add_funcions	add_moduls	add_menu	add_conf					set_perms

noconf:	define_main	add_funcions	add_moduls	add_menu										set_perms

clean:
	@rm ${TARGET_FILE}
	@rm ${TARGET_CONF}
	@rm ${HOME}/${TARGET_FILE}
	@rm ${HOME}/${TARGET_CONF}

define_main:
	@echo -en "Capçalera..."
	@echo -e "#!/usr/bin/env bash\n" > ${TARGET_FILE}
	@echo -e "####### Usuari administrador ############" >> ${TARGET_FILE}
	@echo -e "admin_user='"$(USER)"'" >> ${TARGET_FILE}
	@echo -e "" >> ${TARGET_FILE}
	@cat "${PRJ_CAP}" >> ${TARGET_FILE}
	@echo -e "\n" >> ${TARGET_FILE}
	@echo -e "OK"

add_funcions:
	@echo -en "Funcions..."
	@for filename in $${PRJ_FNC[*]}; do cat $${filename} >> ${TARGET_FILE}; echo -e "\n" >> ${TARGET_FILE}; echo >> ${TARGET_FILE}; done
	@echo -e "OK"

add_moduls:
	@echo -en "Mòduls..."
	@for filename in $${PRJ_MOD[*]}; do cat $${filename} >> ${TARGET_FILE}; echo -e "\n" >> ${TARGET_FILE}; echo >> ${TARGET_FILE}; done
	@echo -e "OK"

add_menu:
	@echo -en "Menú inicial..."
	@echo -e "\n" >> ${TARGET_FILE}
	@cat "${PRJ_MNU}" >> ${TARGET_FILE}
	@echo -e "\n" >> ${TARGET_FILE}
	@echo -e "OK"

set_perms:
	@echo -en "Permisos..."
	@chmod +x "${PWD}"/"${TARGET_FILE}"
	@echo -e "OK"

add_conf:
	@echo -en "Configuració criptomoneda..."
	@cat "${PRJ_CNF}" > ${TARGET_CONF}
	@echo -e "OK"
	
add_conf_bitcoin:
	@echo -en "Configuració Bitcoin..."
	@cat "${PRJ_CNF}" > ${TARGET_CONF}
	@sed -i -e 's|mycoin|bitcoin|' ${TARGET_CONF}
	@echo -e "OK"

install:
	@echo -en "Instaŀlació..."
	@cp ${TARGET_FILE} ${HOME}/
	@cp ${TARGET_CONF} ${HOME}/
	@echo -e "OK"
