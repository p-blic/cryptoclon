# Paràmetres
Els valors dels paràmetres necessaris per configurar la nova criptomoneda s'emmagatzemen al fitxer **coin.conf**.

[[_TOC_]]

## Paràmetres generals
#### nom_moneda

És el nom que tindrà la nova moneda. Si és "**bitcoin**" s'ignoraran la resta de paràmetres i es crearà un node operatiu de Bitcoin.

* Requeriments:
    * Només una paraula
    * Tot en minúscules
* Valor per defecte: **'mycoin'**
***
#### path_moneda

Directori de treball. És el lloc on es baixarà el codi font i les aplicacions necessàries per executar els diferents processos.

* Requeriments:
    * Preferible sense espais ni caràcters especials.
    * No guardar res aquí, ja que el contingut d'aquest directori s'esborra al fer una neteja.
* Valor per defecte: **'LaTevaMoneda'**

***
#### bdb_48

Indica si instal·larà la versió 4.8 de Berkeley DB, per mantenir la compatibilitat amb Bitcoin.Aquestt valor normalment no es modifica.

* Requeriments:
    * Ha de ser un d'aquests valors:
        * **0**: Instal·lar la versió més actual.
        * **1**: Instal·lar la versió 4.8
* Valor per defecte: **1**
***
#### git_remote

Repositori a clonar. Per crear una nova moneda a partir del codi de Bitcoin, deixar el valor buit. En cas de tenir un repositori propi, posar l'URL aquí.

* Requeriments:
    * La ruta ha de ser completa (https://el/teu/repositori.git)
* Valor per defecte:
    * **''**

## Prefixos de les adreces
Aquests prefixos s'afegeixen a l'inici de les adreces per indicar quin tipus de script s'utilitzarà a les transaccions. Es fan servir els scripts següents:

* **Pay to PubKey Hash** (P2PKH)[^1]: És l'script més comú i s'utilitza per enviar coins. Bloqueja (enllaça) una sortida al hash d'una clau pública.
* **Pay to Script Hash** (P2SH)[^2]: Permet bloquejar (enllaçar) coins al hash d'un script, en lloc del hash de una clau pública. Això fa que les coins enviades no es puguin gastar fins que l'script retorni un resultat de "verdader" (`TRUE`).

Els prefixos, igual que les adreces, estan codificats en [base58](https://ca.wikipedia.org/wiki/Base58). Per escollir un prefix personalitzat es pot consultar aquesta [taula](https://en.bitcoin.it/wiki/List_of_address_prefixes).

* **Bech32** (P2PKH i P2SH): Amb la activació del *segregated witness*[^3] s'ha introduït un altre tipus d'adreça anomenat *Bech32*[^4] que també porta un prefix.

***
#### p2pkh_main
Pay to Pubkey Hash (Mainnet)

* Requeriments:
    * Nombre decimal que es pugui codificar com un caràcter en base58.
* Valor per defecte:
    * **'53'**
***

#### p2sh_main
Pay to Script Hash (Mainnet)

* Requeriments:
    * Nombre decimal que es pugui codificar com un caràcter en base58.
* Valor per defecte:
    * **'48'**
***
#### b32_main
Pay to Pubkey Hash (Mainnet)

* Requeriments:
    * Cadena entre 1 i 83 caràcters de la codificació [US-ASCII](https://ca.wikipedia.org/wiki/ASCII) i rang entre 33 i 126 decimal[^5].
* Valor per defecte:
    * **'mm'**
***

#### p2pkh_test
Pay to Pubkey Hash (Testnet)

* Requeriments:
    * Nombre decimal que es pugui codificar com un caràcter en base58.
* Valor per defecte:
    * **'112'**
***

#### p2sh_test
Pay to Script Hash (Testnet)

* Requeriments:
    * Nombre decimal que es pugui codificar com un caràcter en base58.
* Valor per defecte:
    * **'115'**
***
#### b32_test
Pay to Pubkey Hash (Testnet)

* Requeriments:
    * Cadena entre 1 i 83 caràcters de la codificació [US-ASCII](https://ca.wikipedia.org/wiki/ASCII) i rang entre 33 i 126 decimal[^5].
* Valor per defecte:
    * **'mb'**
***

#### p2pkh_reg
Pay to Pubkey Hash (Regtest)

* Requeriments:
    * Nombre decimal que es pugui codificar com un caràcter en base58.
* Valor per defecte:
    * **'112'**
***

#### p2sh_reg
Pay to Script Hash (Regtest)

* Requeriments:
    * Nombre decimal que es pugui codificar com un caràcter en base58.
* Valor per defecte:
    * **'115'**
***
#### b32_reg
Pay to Pubkey Hash (Regtest)

* Requeriments:
    * Cadena entre 1 i 83 caràcters de la codificació [US-ASCII](https://ca.wikipedia.org/wiki/ASCII) i rang entre 33 i 126 decimal[^5].
* Valor per defecte:
    * **'mmrt'**

## Prefixos dels missatges
Tots els missatges enviats entre nodes comencen amb un prefix de quatre bytes predefinits, anomenats "*Magic bytes*". El rang a utilitzar està a la part superior de la taula ASCII i que no forma un caràcter UTF8 vàlid (0x80 a 0xff)[^6].
***
#### pm0_main
Primer byte del prefix de missatge (Mainnet).

* Requeriments:
    * De 0x80 a 0xff
* Valor per defecte:
    * **0xaa**
***
#### pm1_main
Segon byte del prefix de missatge (Mainnet).

* Requeriments:
    * De 0x80 a 0xff
* Valor per defecte:
    * **0xa0**
***
#### pm2_main
Tercer byte del prefix de missatge (Mainnet).

* Requeriments:
    * De 0x80 a 0xff
* Valor per defecte:
    * **0xab**
***
#### pm3_main
Quart byte del prefix de missatge (Mainnet).

* Requeriments:
    * De 0x80 a 0xff
* Valor per defecte:
    * **0xa1**
***
#### pm0_test
Primer byte del prefix de missatge (Testnet).

* Requeriments:
    * De 0x80 a 0xff
* Valor per defecte:
    * **0xba**
***
#### pm1_test
Segon byte del prefix de missatge (Testnet).

* Requeriments:
    * De 0x80 a 0xff
* Valor per defecte:
    * **0xb0**
***
#### pm2_test
Tercer byte del prefix de missatge (Testnet).

* Requeriments:
    * De 0x80 a 0xff
* Valor per defecte:
    * **0xbb**
***
#### pm3_test
Quart byte del prefix de missatge (Testnet).

* Requeriments:
    * De 0x80 a 0xff
* Valor per defecte:
    * **0xb1**
***
#### pm0_reg
Primer byte del prefix de missatge (Regtest).

* Requeriments:
    * De 0x80 a 0xff
* Valor per defecte:
    * **0xca**
***
#### pm1_reg
Segon byte del prefix de missatge (Regtest).

* Requeriments:
    * De 0x80 a 0xff
* Valor per defecte:
    * **0xc0**
***
#### pm2_reg
Tercer byte del prefix de missatge (Regtest).

* Requeriments:
    * De 0x80 a 0xff
* Valor per defecte:
    * **0xcb**
***
#### pm3_reg
Quart byte del prefix de missatge (Regtest).

* Requeriments:
    * De 0x80 a 0xff
* Valor per defecte:
    * **0xc1**
***
## Ports de comunicació
Són els ports mitjançant els quals els nodes es comuniquen amb altres nodes i aplicacions.

* Port **RPC**: Accepta comandaments i envia respostes mitjançant RPC-JSON
* Port **P2P**: Els nodes fan servir aquest port per comunicar-se entre ells.

#### rpc_main
Port RCP (Mainnet)

* Requeriments:
    * El rang del port ha de ser entre 1080 i 65535.
* Suggeriments:
    * És preferible verificar que el port utilitzat no el fa servir cap altre aplicació ([llista de ports](https://en.wikipedia.org/wiki/List_of_TCP_and_UDP_port_numbers) ).
* Valor per defecte:
    * **38332**
***
#### p2p_main
Port P2P (Mainnet).

* Requeriments:
    * El rang del port ha de ser entre 1080 i 65535.
* Suggeriments:
    * És preferible verificar que el port utilitzat no el fa servir cap altre aplicació ([llista de ports](https://en.wikipedia.org/wiki/List_of_TCP_and_UDP_port_numbers) ).
* Valor per defecte:
    * **38333**
***
#### rpc_test
Port RCP (Testnet)

* Requeriments:
    * El rang del port ha de ser entre 1080 i 65535.
* Suggeriments:
    * És preferible verificar que el port utilitzat no el fa servir cap altre aplicació ([llista de ports](https://en.wikipedia.org/wiki/List_of_TCP_and_UDP_port_numbers) ).
* Valor per defecte:
    * **38433**
***
#### p2p_test
Port P2P (Testnet).

* Requeriments:
    * El rang del port ha de ser entre 1080 i 65535.
* Suggeriments:
    * És preferible verificar que el port utilitzat no el fa servir cap altre aplicació ([llista de ports](https://en.wikipedia.org/wiki/List_of_TCP_and_UDP_port_numbers) ).
* Valor per defecte:
    * **38434**
***
#### rpc_reg
Port RCP (Regtest)

* Requeriments:
    * El rang del port ha de ser entre 1080 i 65535.
* Suggeriments:
    * És preferible verificar que el port utilitzat no el fa servir cap altre aplicació ([llista de ports](https://en.wikipedia.org/wiki/List_of_TCP_and_UDP_port_numbers) ).
* Valor per defecte:
    * **38532**
***
#### p2p_reg
Port P2P (Regtest).

* Requeriments:
    * El rang del port ha de ser entre 1080 i 65535.
* Suggeriments:
    * És preferible verificar que el port utilitzat no el fa servir cap altre aplicació ([llista de ports](https://en.wikipedia.org/wiki/List_of_TCP_and_UDP_port_numbers) ).
* Valor per defecte:
    * **38533**
***
#### rpc_user
Nom d'usuari per connectar amb el node mitjançant RCP

* Requeriments:
    * Sense espais ni caràcters especials. Preferible només ASCII.
* Valor per defecte:
    * '**cryptoclon**'
***
#### rpc_pass
Contrasenya de l'usuari per accés RPC.

* Requeriments:
    * Contrasenya forta (Majúscules, minúscules, símbols, números)
    * **No fer servir la contrasenya per defecte. Sempre posar una de diferent.**
* Valor per defecte:
    * '**8576hgnb*FED$**'
***
## Identitat
Aquests són els paràmetres que faran que la nova criptomoneda sigui diferent del Bitcoin, encara que estigui basada en el mateix protocol.

#### nSubsidy
Recompensa inicial per cada bloc minat.

* Requeriments:
    * Nombre enter
* Valor per defecte:
    * **100**
***
#### nSubsidyHalvingInterval_main
Nombre de blocs en el que es produeix un [halving](https://en.wikipedia.org/wiki/Division_by_two) i la recompensa per bloc es redueix a la meitat (Mainnet).

* Requeriments:
    * Nombre enter
* Valor per defecte:
    * **150000**
***
#### nSubsidyHalvingInterval_test
Nombre de blocs en el que es produeix un [halving](https://en.wikipedia.org/wiki/Division_by_two) i la recompensa per bloc es redueix a la meitat (Testnet).

* Requeriments:
    * Nombre enter
* Valor per defecte:
    * **150000**
***
#### max_block_weight
És el pes màxim de cada bloc. No confondre amb la mida del bloc, que és un paràmetre diferent. Cada unitat de pes representa *1/max_block_weight* de la mida del bloc [^7].

* Requeriments:
    * Nombre enter
* Valor per defecte:
    * **4000000**
***
#### coinbase_maturity
Les criptomonedes rebudes com a recompensa per minar un bloc no podran ser gastades fins que hagin transcorregut la quantitat de blocs indicada per aquest paràmetre [^8].

* Requeriments:
    * Nombre enter
* Valor per defecte:
    * **100**
***
#### powlimit_main
Màscara que representa una dificultat d'1, o sigui, el mínim objectiu necessari per portar a terme la prova de treball (PoW) (Mainnet).

* Requeriments:
    * Nombre hexadecimal de 64 números sense el prefix "0x".
* Suggeriments:
    * Aquest valor s'ha d'anar actualitzant conforme la cadena de blocs va creixent. A l'inici, convé mantenir-lo baix.
* Valor per defecte:
    * **00ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff**
***
#### powlimit_test
Màscara que representa una dificultat d'1, o sigui, el mínim objectiu necessari per portar a terme la prova de treball (PoW) (Testnet).

Requeriments:
    * Nombre hexadecimal de 64 números (256 bits) sense el prefix "0x".
* Suggeriments:
    * Aquest valor s'ha d'anar actualitzant conforme la cadena de blocs va creixent. A l'inici, convé mantenir-lo baix.
* Valor per defecte:
    * '**00ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff**'
***
#### nMinimumChainWork_main
És la quantitat de treball que s'ha realitzat en el moment de revisar el software (Mainnet). En el cas d'una nova moneda s'ha de posar un valor baix (com a màxim la dificultat del bloc inicial -1). No és un paràmetre crític, però convé'actualitzar-lo periòdicament.

* Requeriments:
    * Nombre hexadecimal de 64 números sense el prefix "0x".
* Valor per defecte:
    * '**0000000000000000000000000000000000000000000000000000000000000101**'
***
#### nMinimumChainWork_test
És la quantitat de treball que s'ha realitzat en el moment de revisar el software (Testnet). En el cas d'una nova moneda s'ha de posar un valor baix (com a màxim la dificultat del bloc inicial -1). No és un paràmetre crític, però convé actualitzar-lo periòdicament.

* Requeriments:
    * Nombre hexadecimal de 64 números sense el prefix "0x".
* Valor per defecte:
    * '**0000000000000000000000000000000000000000000000000000000010010001**'
***

#### nPowTargetSpacing_main
Quantitat de temps esperada per minar un bloc, expressat en minuts (Mainnet).

* Requeriments:
    * Nombre enter
* Valor per defecte:
    * **5**
***
#### nPowTargetSpacing_test
Quantitat de temps esperada per minar un bloc, expressat en minuts (Testnet).

* Requeriments:
    * Nombre enter
* Valor per defecte:
    * **5**
***
#### nPowTargetSpacing_reg
Quantitat de temps esperada per minar un bloc, expressat en minuts (Regtest).

* Requeriments:
    * Nombre enter
* Valor per defecte:
    * **5**
***
#### nPowTargetTimespan_main
Interval de temps pel recàlcul de dificultat, expressat en dies (Mainnet).
En realitat l'interval de recàlcul es mesura en blocs seguint aquesta fórmula (és per mainnet, però serveix igual per les altres xarxes):

    DifficultyAdjustmentInterval = (nPowTargetTimespan_main * 24 * 60 * 60)/ (nPowTargetSpacing_main * 60)

* Requeriments:
    * Nombre enter
* Valor per defecte:
    * **5**
***
#### nPowTargetTimespan_test
Interval de temps pel recàlcul de dificultat, expressat en dies (Testnet).

* Requeriments:
    * Nombre enter
* Valor per defecte:
    * **5**
***
#### nPowTargetTimespan_reg
Interval de temps pel recàlcul de dificultat, expressat en dies (Regtest).

* Requeriments:
    * Nombre enter
* Valor per defecte:
    * **5**
***
## Bloc inicial
Aquests paràmetres defineixen el bloc inicial de la nova criptomoneda. Es poden deixar buits per aplicar els valors per defecte i crear un bloc 0 ràpidament, però és millor personalitzar-ho i tenir una cadena única [^9]. Un cop minat el bloc inicial, els seus valors es poden posar en aquest fitxer de configuració per tal de no haver de generar-lo cada cop que es compili el codi font.
#### pszTimestamp
Text que posarem al coinbase del bloc inicial.

* Requeriments:
    * Cadena de fins a 91 caràcters ASCII.
* Suggeriments:
    * Aquest text normalment serveix per datar el naixement de la cadena de blocs de la criptomoneda, per això acostuma a ser una notícia o dada rellevant del dia en el que es produeix el minat.
* Valor per defecte:
    * ''
***
#### pubkey
La clau pública de l'adreça receptora de la recompensa pel minat del bloc.

* Requeriments:
    * Nombre hexadecimal de 130 caràcters (520 bits) sense el prefix "0x"
* Suggeriments:
    * Cryptoclon genera un parell de claus privada/pública i les guarda en un fitxer.
* Valor per defecte:
    * '**04d646059440c5138448085f2b2015692d8949fabc654162d207b5d80ad1f0ca6b5b3dfed72efe857765c24966c0afcee971681e06de6c3e28911b6f4d0d7a255a**'
***
#### nBits_hex
Capçalera del hash de la dificultat..

* Requeriments:
    * Nombre hexadecimal de 8 caràcters (32 bits). S'ha d'incloure el prefix "0x"
* Suggeriments:
    * Aquest valor no s'acostuma a modificar, però es pot fer amb precaució per disminuir la dificultat.
* Valor per defecte:
    * '**0x1d00ffff**'
***
#### nTime
Data i hora de minat del bloc.

* Requeriments:
    * Data i hora en format [Unix timestamp](https://www.unixtimestamp.com/index.php)
* Suggeriments:
    * Si no es disposa d'un bloc pre-minat, és recomanable deixar aquest paràmetre a 0
* Valor per defecte:
    * '**1597512051**'
***
#### genesisReward
Recompensa pel minat del bloc inicial expressat en unitats de la criptomoneda.

* Requeriments:
    * Nombre enter
* Suggeriments:
    * Aquest paràmetre normalment te el mateix valor que **nSubsidy**.
* Valor per defecte:
    * **100**
***
#### startNonce
El *nonce* és el número que s'ha d'afegir al hash d'un bloc ja xifrat de tal manera que, un cop recalculat el hash, compleix amb el nivell de dificultat demanat.

* Requeriments:
    * Nombre decimal amb un rang de 32 bits sense signe (0 a 4294967295)
* Suggeriments:
    * Per noves criptomonedes, aquest paràmetre acostuma a valer 0
* Valor per defecte:
    * '**1078218270**'
***


***
[^1]: Pay to Pubkey Hash: [Ref. 1](https://en.bitcoinwiki.org/wiki/Pay-to-Pubkey_Hash). [Ref 2](https://learnmeabitcoin.com/technical/p2pkh)
[^2]: Pay to Script Hash: [Ref. 1](https://en.bitcoin.it/wiki/Pay_to_script_hash). [Ref 2](https://learnmeabitcoin.com/technical/p2sh)
[^3]: Segregated witness: [Ref. 1](https://en.bitcoin.it/wiki/Segwit). [Ref. 2](https://learnmeabitcoin.com/faq/segregated-witness)
[^4]: [Adreces bech32](https://en.bitcoin.it/wiki/Bech32)
[^5]: [Prefix per adreces bech32](https://en.bitcoin.it/wiki/BIP_0173#Bech32)
[^6]: [Prefixos dels missatges](https://learnmeabitcoin.com/technical/magic-bytes)
[^7]:Pes del bloc: [Ref. 1](https://en.bitcoinwiki.org/wiki/Block_weight). [Ref. 2](https://learnmeabitcoin.com/technical/transaction-weight). [Ref. 3](https://en.bitcoin.it/wiki/Weight_units)
[^8]: [Maduresa de la recompensa (coinbase_maturity)](https://bitcoin.stackexchange.com/a/1992)
[^9]: [Bloc inicial](https://en.bitcoinwiki.org/wiki/Genesis_block) 