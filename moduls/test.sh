##############################
# Execució de prova
function test_node() {
	trap exit_handler ERR
	if pgrep -x "$nom_moneda"d >> "$file_log"; then
		echo -e "$BRed""Ja hi ha un node engegat. Aturant..."
		kill $(pidof "$nom_moneda"d) >&3 2>&4
		if [ $? != 0 ]; then
			echo -e"$BRed""No s'ha pogut aturar el node. Sortint de l'aplicació"
			exit 5
		fi
	fi
	cd "$git_path"
	if [ -f src/"$nom_moneda"d ] && [ -x src/"$nom_moneda"d ]; then
		while true; do
			msg_test_node
			echo -en "$Yellow"
			read -p "Començar (S/N)?" yn
			echo -e "$Color_Off"
			case $yn in
				[Ss]* ) echo -e $Yellow
					echo -e $Color_Off
					trap - ERR
					timeout 90s src/"$nom_moneda"d -regtest 2>&1 | tee "$script_path"/nodetest.log
					trap exit_handler ERR
					msg_test_node_fi
					pausa_tecla
					break;;
				[Nn]* ) break;;
				* ) echo -e "$BRed Ha de respondre sí o no.";;
			esac
		done
	else
		msg_test_nocomp
		pausa_tecla
	fi
}

