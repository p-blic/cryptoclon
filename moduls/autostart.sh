##############################
# Inici automàtic
function autostart() {
	trap exit_handler ERR
	delete_autostart
	local service_file="/lib/systemd/system/$nom_moneda""d.service"
	echo -e "$Yellow-Servei d'arrencada automàtica del node...$Color_Off"
	if [ -f "$service_file" ]; then
		msg_service_exists >&3 2>&4
		sudo rm "$service_file"
	fi
	echo -e "$Green       Insertant contingut...$Color_Off"
	cd "$git_path"
	sudo cp contrib/init/"$nom_moneda"d.service "$service_file" >&3 2>&4
	sudo ln -s "$service_file" "/etc/systemd/system/$nom_moneda""d.service"
	sudo sed -i -e 's|/usr/bin/|/usr/local/bin/|g' "$service_file"
	echo -e "$Green       $service_file...OK"
	echo -e "$Green       Habilitant el servei...$Color_Off"
	sudo chown root:root "$service_file" >&3 2>&4
	sudo chmod 644 "$service_file" >&3 2>&4
	sudo systemctl daemon-reload  >&3 2>&4
	sudo systemctl enable "$nom_moneda"d.service >&3 2>&4
	if [ ! -d /run/"$nom_moneda"d ]; then
		sudo mkdir /run/"$nom_moneda"d >&3 2>&4
		sudo chown "$nom_moneda":"$nom_moneda" /run/"$nom_moneda"d >&3 2>&4
	fi
	if [ ! -d /var/lib/"$nom_moneda"d ]; then
		sudo mkdir /var/lib/"$nom_moneda"d >&3 2>&4
		sudo chown "$nom_moneda":"$nom_moneda" /var/lib/"$nom_moneda"d >&3 2>&4
	fi
	echo -e "$Green       ...OK"
	msg_service_fi
	sudo systemctl start "$nom_moneda"d.service >&3 2>&4
}