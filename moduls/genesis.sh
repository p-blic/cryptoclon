##############################
# Bloc inicial
function genesis() {
	trap exit_handler ERR
	cd "$coin_path"
	echo -e "$Yellow-Bloc inicial (Genesis block)...$Color_Off"
	echo -e "$Green       Comprobant si existeix el programa per minar...$Color_Off"
	if [[ ! -d "genesisgen" ]]; then
		echo -e "$Green       ...No hi ha. Clonant codi font...$Color_Off"
		git clone 'https://gitlab.com/p-blic/genesisgen' >&3 2>&4
		cd genesisgen
		make >&3 2>&4
		cd ..
		cp genesisgen/genesis .
	fi
	echo -e "$Green       Comprobant executable...$Color_Off"
	if [[ ! -f "genesis" ]]
	then
		echo -e "$BRed    ...El fitxer genesis no existeix. Sortint de l'aplicació.$Color_Off"
		exit 2
	else
		echo -e "$Green       ...Executable existent$Color_Off"
	fi

##############################
# Parell de claus
	echo -e "$Yellow-Parell de claus$Color_Off"
	echo -e "$Green       Generant claus...$Color_Off"
	if [[ "$pubkey" != '' ]]; then
		echo -e "$Green       ...Clau pública existent. No es generarà una de nova$Color_Off"
	else
		echo -e "$Green       ...No hi ha claus definides. Es generen unes de noves$Color_Off"
		openssl ecparam -genkey -name secp256k1 -out secp256k1.priv >&3 2>&4
		openssl ec -in secp256k1.priv -pubout -out secp256k1.pub >&3 2>&4
		privkey=$(openssl ec -in secp256k1.priv -outform DER|tail -c +8|head -c 32|xxd -p -c 32)
		pubkey=$(openssl ec -in secp256k1.priv -pubout -outform DER|tail -c 65|xxd -p -c 65)
		echo Clau privada: "$privkey" > "$nom_moneda".keys
		echo Clau pública: "$pubkey" >> "$nom_moneda".keys
		rm -f secp256k1.priv
		rm -f secp256k1.pub
		echo -e "$Green       El fitxer $nom_moneda.keys conté les claus noves$Color_Off"
	fi

##############################
# Minat del bloc inicial
	echo -e "$Yellow-Minat del bloc inicial...$Color_Off"
	msg_genesis_inici	
#Genera executable per reproduir el minat.
	echo -e "$Green       Generant script executable per repetir el minat si és necessari$Yellow"
	echo "#!/usr/bin/env bash" > "$nom_moneda".sh
	echo "" >> "$nom_moneda".sh
	echo "./genesis "$pubkey" \""$pszTimestamp"\"  "$nBits_dec" "$startNonce" "$nTime" "$genesisReward"" >> "$nom_moneda".sh
	chmod +x "$nom_moneda".sh
# Minar Genesis Block
#	while true; do
#		read -p "Continuar (S/N)?" yn
#		case $yn in
#			[Ss]* ) echo -e "$Cyan$doble_guion$Color_Off"
				echo -e "$Cyan$doble_guion$Color_Off"
				./genesis "$pubkey" "$pszTimestamp"  "$nBits_dec" "$startNonce" "$nTime" "$genesisReward"
				for j in "$nTime".*; do mv -- "$j" "${nom_moneda}.txt"; done
				echo -e "$Cyan$doble_guion$Color_Off"
#				break;;
#			[Nn]* ) exit;;
#			* ) echo -e "$BRed Ha de respondre sí o no.";;
#		esac
#	done
	if [[ ! -f "$nom_moneda.txt" ]]
	then
		echo -e "$BRed    El fitxer de paràmetres per el bloc inicial no s'ha generat. Sortint de l'aplicació.$Color_Off"
		exit 3
	fi
	echo -e "$Green       ...OK"
	echo ""
	msg_genesis_fin
	echo -e $Yellow
	#read -n 1 -s -r -p "Llegeix el text anterior i pulsa qualsevol tecla per continuar"
	echo -e $Color_Off
	echo ""

##############################
# Paràmetres del bloc inicial
	echo -e "$Yellow-Injectant paràmetres minats al bloc inicial...$Color_Off"
	source "$nom_moneda.txt"
	cd "$git_path"/src
	file="chainparams.cpp"
# Paràmetres comuns
	gb_com_dst=( '<< '"$nBits_dec"' <<' "$xTimestamp" "$xPubkey" )
	for ((i=0;i<${#gb_com_src[@]};++i)); do
		sed -i -e 's|'"${gb_com_src[i]}"'|'"${gb_com_dst[i]}"'|g' "$file"
	done
# Mainnet
	gb_main_dst=( "($xUnixTime" " $xNonce" " $xnBits" ', '"$xGenesisReward"' ' "$xHash" "$xByteswappedMerkleHash" )
	for ((i=0;i<${#gb_main_src[@]};++i)); do
		sed -i -e 's|'"${gb_main_src[i]}"'|'"${gb_main_dst[i]}"'|g' "$file"
	done
# Testnet
	gb_test_dst=( "($xUnixTime" " $xNonce," " $xnBits" ', '"$xGenesisReward"' ' "$xHash" "$xByteswappedMerkleHash" )
	for ((i=0;i<${#gb_test_src[@]};++i)); do
		sed -i -e 's|'"${gb_test_src[i]}"'|'"${gb_test_dst[i]}"'|g' "$file"
	done
# Regtest
	gb_regt_dst=( "($xUnixTime" " $xNonce," " $xnBits" ', '"$xGenesisReward"' ' "$xHash" "$xByteswappedMerkleHash" )
	for ((i=0;i<${#gb_regt_src[@]};++i)); do
		sed -i -e 's|'"${gb_regt_src[i]}"'|'"${gb_regt_dst[i]}"'|g' "$file"
	done
	for ((i=0;i<${#bip_hash_src[@]};++i)); do
		sed -i -e 's|'"${bip_hash_src[i]}"'|0x'"$xHash"'|g' "$file"
	done
	echo -e "$Green       $file...OK"
	status_coin
}