##############################
# Reset
# Verificar que el usuario $moneda existe
function reset_coin() {
	trap exit_handler ERR
	#local user_path="/home/$nom_moneda"
	while true; do
		echo -e "$BRed"
		read -p "Fer un RESET TOTAL(S/N)?" yn
		case $yn in
			[Ss]* )
				msg_reset
				while true; do
					echo -en "$BRed"
					read -p "Continuar (S/N)?" yn
					case $yn in
						[Ss]* ) #echo -e "\n$proc_verd       Netejant i esborrant...$Color_Off"
							cd "$script_path"
####### Desinstaŀlar engegada automàtica
							delete_autostart
							status_coin
							echo -e "$Yellow-Esborrar fitxers...$Color_Off"
####### Desinstaŀlar criptomoneda
							echo -e "$proc_verd""Executables...$Color_Off"
							sudo rm -rf /usr/local/bin/*"$nom_moneda"* >&3 2>&4
####### Esborrar fitxers
							echo -e "$proc_verd""Fitxers d'usuari...$Color_Off"
							if id "$nom_moneda" >/dev/null 2>&1; then #usuario existe
								#cd "$user_path"
								rm -rf "$coin_path"
								#sudo find . -maxdepth 1 -type d -name '*' ! -name '.*' -exec sudo rm -rf {} +
								sudo find . -type f ! -name "$script_name.*" ! -name "coin.conf" -delete
								sudo rm -rf .bitcoin/ >&3 2>&4
								#rm -rf "*.log" >&3 2>&4
							fi
							echo -e "$proc_verd""Ubicació de dades...$Color_Off"
							sudo rm -rf /var/lib/"$nom_moneda"d >&3 2>&4
							sudo rm -rf /etc/"$nom_moneda" >&3 2>&4							
####### Desinstaŀlar LND
							echo -e "$proc_verd""Lightning network...$Color_Off"
							if [[ -f "/usr/local/go/bin/go" ]]; then
								sudo rm -rf /usr/local/go/  >&3 2>&4	
							fi
							sudo rm -rf ~/gocode/ >&3 2>&4	
							echo -e "$proc_verd...OK$Color_Off"
							status_coin
							break;;
						[Nn]* ) return 0
							break;;
						* ) echo -e "$BRed Ha de respondre sí o no.";;
					esac
				done
				break;;
			[Nn]* ) return 0
				break;;
			* ) echo -e "$BRed Ha de respondre sí o no.";;
		esac
	done
	echo ""
}