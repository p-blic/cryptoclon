##############################
# Identitat
#
function identitat() {
	trap exit_handler ERR
	cd "$git_path"
	echo -e -n "$Green       Procesant bitcoin -> $nom_moneda...$Color_Off"
	find -type f -not -path "./.git/*" -exec sed -i 's/bitcoin/'"$nom_moneda"'/g' {} + &
	spinner $!
	echo -e "\n$Green       ...OK$Color_Off"
	echo -e -n "$Green       Procesant Bitcoin -> $nom_moneda_Mm...$Color_Off"
	find -type f -not -path "./.git/*" -exec sed -i 's/Bitcoin/'"$nom_moneda_Mm"'/g' {} + &
	spinner $!
	echo -e "\n$Green       ...OK$Color_Off"
	echo -e -n "$Green       Procesant BITCOIN -> $nom_moneda_MM...$Color_Off"
	find -type f -not -path "./.git/*" -exec sed -i 's/BITCOIN/'"$nom_moneda_MM"'/g' {} + &
	spinner $!
	echo -e "\n$Green       ...OK$Color_Off"
	echo -e -n "$Green       Procesant fitxers bitcoin* -> $nom_moneda*...$Color_Off"
	find . -not -path "./.git/*" -iname "bitcoin*" -exec rename 's/bitcoin/'"$nom_moneda"'/' '{}' \; >&3 2>&4 &
	spinner $!
	echo -e "\n$Green       ...OK$Color_Off"
	echo -e -n "$Green       Procesant fitxers *bitcoin* -> *$nom_moneda*...$Color_Off"
	find . -not -path "./.git/*" -iname "*bitcoin*" -exec rename 's/bitcoin/'"$nom_moneda"'/' '{}' \; >&3 2>&4 &
	spinner $!
	echo -e "\n$Green       ...OK$Color_Off"				
	echo -e -n "$Green       Segona passada bitcoin* -> menutcoin* i *bitcoin* -> *menutcoin* $Color_Off"
	find . -not -path "./.git/*" -iname "bitcoin*" -exec rename 's/bitcoin/'"$nom_moneda"'/' '{}' \; >&3 2>&4 &
	spinner $!
	find . -not -path "./.git/*" -iname "*bitcoin*" -exec rename 's/bitcoin/'"$nom_moneda"'/' '{}' \; >&3 2>&4 &
	spinner $!
	echo -e "\n$Green       ...OK$Color_Off"
	echo -e -n "$Green       Retornar les referències a l'equip desenvolupador de Bitcoin. $Color_Off"
	find -type f -not -path "./.git/*" -exec sed -i 's/'"$nom_moneda_Mm"' Core developers/Bitcoin Core developers/g' {} + &
	spinner $!
	echo -e "\n$Green       ...OK$Color_Off"
				
##############################
# Prefixos de les adreces
	echo -e "$Yellow-Prefixos de les adreces...$Color_Off"
	cd "$git_path"/src
	file="chainparams.cpp"
	for ((i=0;i<${#p_addr_src[@]};++i)); do
		sed -i -e 's|'"${p_addr_src[i]}"'|'"${p_addr_dst[i]}"'|g' "$file"
	done
	echo -e "$Green       $file...OK"
	echo -e $Color_Off

##############################
# Prefixos dels missatges
	echo -e "$Yellow-Prefixos dels missatges...$Color_Off"
	for ((i=0;i<${#p_msg_src[@]};++i)); do
		sed -i -e 's|'"${p_msg_src[i]}"'|'"${p_msg_dst[i]}"'|g' "$file"
	done
	echo -e "$Green       $file...OK"
	echo -e $Color_Off

##############################
# Ports de comunicació
#	P2P
	echo -e "$Yellow-Ports de comunicació...$Color_Off"
	for ((i=0;i<${#p_p2p_src[@]};++i)); do
		sed -i -e 's|'"${p_p2p_src[i]}"'|'"${p_p2p_dst[i]}"'|g' "$file"
	done
	echo -e "$Green       $file...OK"
#	RPC
	file="chainparamsbase.cpp"	#RPC
	for ((i=0;i<${#p_rpc_src[@]};++i)); do
		sed -i -e 's|'"${p_rpc_src[i]}"'|'"${p_rpc_dst[i]}"'|g' "$file"
	done
	echo -e "$Green       $file...OK"
	echo -e $Color_Off

##############################
# Nodes llavor (Seed nodes)
	echo -e "$Yellow-Nodes llavor (Seed nodes)...$Color_Off"
	file="chainparams.cpp"
# Mainnet Testnet
	src='vSeeds.emplace_back('
	dst='//vSeeds.emplace_back('
	trap - ERR
	grep -q "$dst" "$file"
	if [[ $? != 0 ]]; then
		my_sed
	fi
	trap exit_handler ERR
	echo -e "$Green       $file...OK"
	echo -e $Color_Off

##############################
# Consens
	echo -e "$Yellow-Consens...$Color_Off"
	for ((i=0;i<${#cons_main_src[@]};++i)); do
		sed -i -e 's|'"${cons_main_src[i]}"'|'"${cons_main_dst[i]}"'|' "$file"
	done
	for ((i=0;i<${#cons_test_src[@]};++i)); do
		sed -i -e 's|'"${cons_test_src[i]}"'|'"${cons_test_dst[i]}"'|' "$file"
	done
	for ((i=0;i<${#cons_reg_src[@]};++i)); do
		sed -i -e 's|'"${cons_reg_src[i]}"'|'"${cons_reg_dst[i]}"'|' "$file"
	done
	for ((i=0;i<${#cons_fix_main_src[@]};++i)); do
		sed -i -e 's|'"${cons_fix_main_src[i]}"'|'"${cons_fix_main_dst[i]}"'|' "$file"
	done
	for ((i=0;i<${#cons_fix_test_src[@]};++i)); do
		sed -i -e 's|'"${cons_fix_test_src[i]}"'|'"${cons_fix_test_dst[i]}"'|' "$file"
	done
	for ((i=0;i<${#cons_fix_reg_src[@]};++i)); do
		sed -i -e 's|'"${cons_fix_reg_src[i]}"'|'"${cons_fix_reg_dst[i]}"'|' "$file"
	done
	
	
	echo -e "$Green       $file...OK"
##############################################################################################
#if false; then # Al introducir este cambio falla el hash resultante del génesis
#file='amount.h'
#for ((i=0;i<${#cons2_src[@]};++i)); do
#	sed -i -e 's|'"${cons2_src[i]}"'|'"${cons2_dst[i]}"'|g' "$file"
#done
#echo -e "$Green       $file...OK"
#fi
##############################################################################################
	file="validation.cpp"
	for ((i=0;i<${#cons3_src[@]};++i)); do
		sed -i -e 's|'"${cons3_src[i]}"'|'"${cons3_dst[i]}"'|g' "$file"
	done
	echo -e "$Green       $file...OK"
	file='consensus/consensus.h'
	for ((i=0;i<${#cons4_src[@]};++i)); do
		sed -i -e 's|'"${cons4_src[i]}"'|'"${cons4_dst[i]}"'|g' "$file"
	done
	echo -e "$Green       $file...OK"
	echo -e $Color_Off

##############################
# Activar BIPs
	echo -e "$Yellow-Activar BIPs...$Color_Off"
	file="chainparams.cpp"
	for ((i=0;i<${#bip_src[@]};++i)); do
		sed -i -e 's|'"${bip_src[i]}"'|'"${bip_dst[i]}"'|g' "$file"
	done
	echo -e "$Green       $file...OK"
	echo -e $Color_Off

##############################
# Punts de control
	echo -e "$Yellow-Punts de control...$Color_Off"
# Mainnet
	src='{ 11111,'
	dst='/*{ 11111,'
	trap - ERR
	grep_dst='\/\*{ 11111,'
	grep -q "$grep_dst" "$file"
	if [[ $? != 0 ]]; then
		my_sed
	fi
	src='c64632a983")},'
	dst='c64632a983")},*/'
	grep_dst='c64632a983")},\*\/'
	grep -q "$grep_dst" "$file"
	if [[ $? != 0 ]]; then
		my_sed
	fi
# testnet
	src='{0, uint256S("0f9188f'
	dst=' //{0, uint256S("0f9188f'
	grep -q "$dst" "$file"
	if [[ $? != 0 ]]; then
		my_sed
	fi
# Regtest
	src='{546, uint256S'
	dst=' //{546, uint256S'
	grep -q "$dst" "$file"
	if [[ $? != 0 ]]; then
		my_sed
	fi
	trap exit_handler ERR
	for ((i=0;i<${#pnts_src[@]};++i)); do
		sed -i -e 's|'"${pnts_src[i]}"'|'"${pnts_dst[i]}"'|g' "$file"
	done
	echo -e "$Green       $file...OK"
	echo -e $Color_Off

##############################
# Backup seeds
  echo -e "$Yellow-Backup seeds...$Color_Off"
  file="chainparamsseeds.h"
  for ((i=0;i<${#bck_seeds_src[@]};++i)); do
		sed -i -e 's|'"${bck_seeds_src[i]}"'|'"${bck_seeds_dst[i]}"'|g' "$file"
	done
  echo -e "$Green       $file...OK"
	echo -e $Color_Off
	status_coin
}
