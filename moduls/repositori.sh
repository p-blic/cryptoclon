##############################
# Clonat del repositori
function clonar_repositori() {
	trap exit_handler ERR
	echo -e "$Yellow-Repositori...$Color_Off"
	echo -en "$proc_verd""Clonant repositori $git_remote...$Color_Off"
	cd "$coin_path"
	if [ "$git_remote" == '' ]; then
		source_repo='https://github.com/bitcoin/bitcoin.git'
	else
		source_repo="$git_remote"
	fi
	destination_path="$git_path"
	clonar_repo
	echo -e "$proc_verd...OK$Color_Off"
	if [[ ! -d "$git_path" ]]
	then
		echo -e "$BRed El directori $git_path no existeix. Sortint de l'aplicació.$Color_Off"
		exit 1
	fi
	cd "$git_path"
	if [ "$nom_moneda" == "bitcoin" ]; then
		git checkout "$btc_branch" >&3 2>&4
	else
		for branch in `git branch -a | grep remotes | grep -v HEAD | grep -v master `; do
			trap - ERR
	  	git branch --track ${branch#remotes/origin/} $branch >&3 2>&4
			trap exit_handler ERR
		done
		while true; do
			echo ""
			echo -e "Branques actuals:$Color_Off"
			git branch
			echo -e "$Yellow"
			read -p "Branca a carregar ([ENTRAR] per no canviar)?  " branca
			if [ "$branca" != '' ]; then
				echo -e $Color_Off
				git checkout "$branca" >&3 2>&4
					if [ "$?" != 0 ]; then
						echo -e "$BRed Branca $branca no trobada$Color_Off"
					else
						echo -e "$proc_verd...OK$Color_Off"
						break
					fi
			else
				echo -e "$proc_verd...OK$Color_Off"
				break
			fi
		done
	fi
	status_coin
}