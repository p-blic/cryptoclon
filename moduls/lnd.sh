##############################
# Instaŀlació de LND
function lnd() {
	trap exit_handler ERR
	local path_1='PATH=$PATH:/usr/local/go/bin'
	local path_2='GOPATH=~/gocode'
	local path_3='PATH=$PATH:$GOPATH/bin'
	cd "$coin_path"
	echo -e "$Yellow-Instaŀlació de Go...$Color_Off"
	if [[ ! -f "/usr/local/go/bin/go" ]]; then
		rm -rf go* >&3 2>&4
		wget https://dl.google.com/go/go"$go_version".linux-amd64.tar.gz >&3 2>&4
		sudo tar -C /usr/local -xzf go"$go_version".linux-amd64.tar.gz >&3 2>&4
	fi
	if [ ! -d ~/gocode ]; then
		mkdir ~/gocode >&3 2>&4
	fi
	if [ ! -d ~/gocode/bin ]; then
		mkdir ~/gocode/bin >&3 2>&4
	fi
	if ! grep -Fq "$path_3" ~/.profile >&3 2>&4; then
		echo 'export '"$path_1" >> ~/.profile
		echo 'export '"$path_2"  >> ~/.profile
		echo 'export '"$path_3"  >> ~/.profile
		source ~/.profile
	fi
	echo -e "$proc_verd...OK$Color_Off"
	status_coin
	echo -en "$Yellow-Repositori...$Color_Off"
	cd "$coin_path"
	source_repo="$git_lnd"
	destination_path="$lnd_path"
	clonar_repo
	echo -e "$proc_verd""Seleccionar branca $lnd_branch...$Color_Off"
	if [[ ! -d "$lnd_path" ]]
	then
		echo -e "$BRed El directori $lnd_path no existeix. Sortint de l'aplicació.$Color_Off"
		exit 1
	fi
	cd "$lnd_path"
	git checkout "$lnd_branch" >&3 2>&4
	echo -e "$proc_verd...OK$Color_Off"
	status_coin
# paràmetres de compilació
#--bitcoind.dir
#--bitcoind.rpchost
#--bitcoind.rpcuser
#--bitcoind.rpcpass
#--bitcoind.zmqpubrawblock
#--bitcoind.zmqpubrawtx
#--bitcoind.estimatemode
	echo -e "$Yellow-Instaŀlar LND...$Color_Off"
	echo -en "$proc_verd""Netejar...$Color_Off"
	make clean >&3 2>&4 &
	spinner $!
	echo -en "\n$proc_verd""Compilar...$Color_Off"
	make "-j"$(nproc --all) install >&3 2>&4 &
	spinner $!
	echo -e "\n$proc_verd...OK$Color_Off"
#make check falla con go 1.15 (https://github.com/lightningnetwork/lnd/pull/4554)
	status_coin
#crear $HOME/.lnd/lnd.conf
#https://stadicus.github.io/RaspiBolt/raspibolt_40_lnd.html#configuration
#https://github.com/lightningnetwork/lnd/blob/master/sample-lnd.conf


}