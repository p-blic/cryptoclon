##############################
# Instaŀlació de la moneda
function instalar() {
	trap exit_handler ERR
	msg_ins
	cd "$git_path"
	if [ -f src/"$nom_moneda"d ] && [ -x src/"$nom_moneda"d ]; then
		echo -e "$Yellow-Instaŀlació de $nom_moneda...$Color_Off"
		echo -en "$proc_verd$nom_moneda -> Instaŀlació...$Color_Off"
		sudo make install >&3 2>&4 &
		spinner $!
		sudo chown $nom_moneda:$nom_moneda /usr/local/bin/*$nom_moneda*
		sudo chmod g+w /usr/local/bin/*$nom_moneda*
		if [ -f "$script_path"/.profile ]; then
			sed -i -e 's|./'"$script_name".sh'||g' "$HOME"/.profile >&3 2>&4
		fi
		echo -e "\n$proc_verd...OK"
	fi
  status_coin
}
