##############################
# Test de minat
function test_minat() {
	trap exit_handler ERR
	clear
#Comprobar si el node està instaŀlat
	if command -v "$nom_moneda"d &> /dev/null; then
		msg_test_minat_r
	else
		msg_node_no_instalat
		pausa_tecla
		return 0
	fi
#Comprobar daemon
	if systemctl is-active --quiet "$nom_moneda""d.service"; then
		sudo systemctl stop "$nom_moneda"d.service  >&3 2>&4
	fi
#Comprobar si hi ha algun node manual engegat
	if pgrep -x "$nom_moneda"d >> "$file_log"; then
		echo -e "$BRed""Hi ha nodes engegats. Aturant..."
		kill $(pidof "$nom_moneda"d) >&3 2>&4
		sleep 2
		if [ $? != 0 ]; then
			echo -e"$BRed""No s'ha pogut aturar el node. Sortint de l'aplicació"
			exit 5
		else
			echo -e "$Green    ...OK\n"
		fi
	fi
	echo -en "$Yellow""Cadena de blocs: $Cyan"
	if [[ "${options[$choice]}" == *"A)"* ]]; then
		net_option='-chain=regtest'
		echo -e "REGTEST\n"
	else
		net_option='-chain=main'
		echo -e "MAIN\n"
	fi
	cd "$script_path"
	if [[ ! -d "node1" ]]; then
		mkdir node1 node2 node3 >&3 2>&4
	fi
	echo -e "$Yellow""Engegant node 1...$Color_Off"
	"$nom_moneda"d "$net_option" -port=1111 -datadir="$script_path/node1" -rpcport=1112 -daemon >&3 2>&4 &
	sleep 1
	echo -e "$Green    ...OK"
	echo -e "$Yellow""Engegant node 2...$Color_Off"
	"$nom_moneda"d "$net_option" -port=2222 -datadir="$script_path/node2" -rpcport=2223 -daemon >&3 2>&4 &
	sleep 1
	echo -e "$Green    ...OK"
	echo -e "$Yellow""Engegant node 3...$Color_Off"
	"$nom_moneda"d "$net_option" -port=3333 -datadir="$script_path/node3" -rpcport=3334 -daemon >&3 2>&4 &
	sleep 1
	echo -e "$Green    ...OK"
	function node1(){
		"$nom_moneda"-cli "$net_option" -datadir="$script_path/node1" -rpcport=1112 $@
	}
	function node2() {
		"$nom_moneda"-cli "$net_option" -datadir="$script_path/node2" -rpcport=2223 $@
	}
	function node3() {
		"$nom_moneda"-cli "$net_option" -datadir="$script_path/node3" -rpcport=3334 $@
	}
	echo -e "$Yellow""Esperant que els nodes s'acabin d'iniciar....$Green"
	for ((i=20;i>0;--i)); do
		echo -ne "."
		sleep .5
	done
	echo -e "$Green.OK"
	echo ""
	node1 addnode "127.0.0.1:2222" "add"
	node1 addnode "127.0.0.1:3333" "add"
	node2 addnode "127.0.0.1:1111" "add"
	node2 addnode "127.0.0.1:3333" "add"
	node3 addnode "127.0.0.1:1111" "add"
	node3 addnode "127.0.0.1:2222" "add"
	echo -e "$Yellow""Nodes en marxa, s'inicia el minat. Pulsa qualsevol tecla per aturar el procès.$Color_Off"
	echo ""
	blocs_ant=$(node1 getblockcount)
	SECONDS=0
	echo -ne "$Cyan""La cadena de blocs de $nom_moneda conté$BPurple $blocs_ant blocs                                                 \r"
	while true
	do
		blocs=$(node1 getblockcount)
		if [ "$blocs" -gt "$blocs_ant" ]; then
			durada="$(($SECONDS / 60))"
			echo -ne "$Cyan La cadena de blocs de $nom_moneda (Regtest) conté$BPurple $blocs blocs i el darrer ha trigat $durada minuts                              \r"
			blocs_ant="$blocs"
			SECONDS=0
		fi
		trap - ERR
		read -n 1 -t 0.1
		if [ $? = 0 ] ; then return 0; fi
		trap exit_handler ERR
		node1 -generate 1 >&3 2>&4
		trap - ERR
		read -n 1 -t 0.1
		if [ $? = 0 ] ; then return 0; fi
		trap exit_handler ERR
		node2 -generate 1 >&3 2>&4
		trap - ERR
		read -n 1 -t 0.1
		if [ $? = 0 ] ; then return 0; fi
		trap exit_handler ERR
		node3 -generate 1 >&3 2>&4
		trap - ERR
		read -n 1 -t 0.1
		if [ $? = 0 ] ; then return 0; fi
		trap exit_handler ERR
	done
	cd "$coin_path"
}