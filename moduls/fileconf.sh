##############################
# Fitxer de configuració
# 
function fileconf() {
	trap exit_handler ERR
	local config_path="/etc/$nom_moneda" #"$HOME"/."$nom_moneda"
	local config_file="$config_path"/"$nom_moneda".conf
	local auth_file="$script_path/rpc_auth.txt"
	echo -e "$Yellow-Fitxer de configuració $nom_moneda.conf...$Color_Off"
	echo -e "$Green       Insertant contingut...$Color_Off"
	if [ ! -d "$config_path" ]; then
		sudo mkdir $config_path >&3 2>&4
	fi
	sudo chown "$nom_moneda":"$nom_moneda" "$config_path"
	if [ -f "$config_file" ]; then
		msg_conf_exists
		rm "$config_file"
	fi
	cd "$git_path"
	./share/rpcauth/rpcauth.py "$rpc_user" "$rpc_pass" > "$auth_file"
	if [[ ! -f "$auth_file" ]]; then
		echo -e "$BRed    El fitxer d'autorització per RCP no s'ha generat. Sortint de l'aplicació.$Color_Off"
		exit 3
	fi
	sed -i '3,4d' "$auth_file" >&3 2>&4
	sed -i '1d' "$auth_file" >&3 2>&4
	sed -i -e 's|':'|'":'"'|' "$auth_file" >&3 2>&4
	sed -i -e 's|$|'"'"'|' "$auth_file" >&3 2>&4
	source "$auth_file"
	sudo touch "$config_file" >&3 2>&4
	sudo chown "$nom_moneda":"$nom_moneda" "$config_file" >&3 2>&4
	sudo chmod g+w "$config_file" >&3 2>&4
	echo -e "##### Fitxer de configuració de $nom_moneda. Creat amb CryptoClon\n" >"$config_file"
	echo -e "server=1\n" >>"$config_file"
	echo -e "txindex=1\n" >>"$config_file"
	echo -e "listen=1\n" >>"$config_file"
	echo -e "bind=127.0.0.1\n" >>"$config_file"
	echo -e "daemon=1\n" >>"$config_file"
	echo -e "### Connexions\nrpcauth=$rpcauth" >>"$config_file"
	echo -e "zmqpubrawblock=tcp://127.0.0.1:$zmq_block_port" >>"$config_file"
	echo -e "zmqpubrawtx=tcp://127.0.0.1:$zmq_tx_port\n" >>"$config_file"
	echo -e "$Green       $config_file...OK"
}
