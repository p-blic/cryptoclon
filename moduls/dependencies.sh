##############################
# Instaŀlar dependències
function dependencies() {
	trap exit_handler ERR
	echo -e "$Yellow-Instaŀlació de les dependències...$Color_Off"
	echo -e "$proc_verd""Actualització del software existent...$Color_Off"
	echo -en "$proc_verd\t""Update...$Color_Off"
	sudo apt-get update -y >&3 2>&4 &
	spinner $!
	echo -en "\n$proc_verd\t""Upgrade...$Color_Off"
	sudo apt-get upgrade -y >&3 2>&4 &
	spinner $!
	echo -en "\n$proc_verd\t""Autoremove...$Color_Off"
	sudo apt-get autoremove -y >&3 2>&4 &
	spinner $!
	echo -en "\n$proc_verd""Instaŀlant dependències (1/6)...$Color_Off"
	sudo apt-get -y install build-essential autoconf libtool pkg-config autotools-dev automake bsdmainutils python3 >&3 2>&4 &
	spinner $!
	echo -en "\n$proc_verd""Instaŀlant dependències (2/6)...$Color_Off"
	sudo apt-get -y install software-properties-common libssl-dev libevent-dev libboost-all-dev >&3 2>&4 &
	spinner $!
	echo -en "\n$proc_verd""Instaŀlant dependències (3/6)...$Color_Off"
	sudo apt-get -y install libminiupnpc-dev libzmq3-dev libprotobuf-dev protobuf-compiler rename >&3 2>&4 &
	spinner $!
	echo -en "\n$proc_verd""Instaŀlant dependències (4/6)...$Color_Off"
	sudo apt-get -y install libqt5gui5 libqt5core5a libqt5dbus5 qttools5-dev qttools5-dev-tools >&3 2>&4 &
	spinner $!
	echo -en "\n$proc_verd""Instaŀlant dependències (5/6)...$Color_Off"
	sudo apt-get -y install doxygen gettext >&3 2>&4 &
	spinner $!
	echo -en "\n$proc_verd""Instaŀlant dependències (6/6)...$Color_Off"
	sudo apt-get -y install libqrencode-dev libunivalue-dev libminiupnpc-dev >&3 2>&4 &
	spinner $!
	echo -e "\n$proc_verd...OK$Color_Off"

#############################
# Instaŀlar Berkeley DB
	if [ "$USER" != "$nom_moneda" ]; then
		msg_usuari_bdb
		echo -en "$Yellow"
		read -n 1 -s -r -p "Polsa una tecla per continuar"
		return 0
	fi
	echo -e "$Yellow-Instaŀlació de Berkeley DB...$Color_Off"
	cd "$coin_path"
	if [ "$bdb_48" -eq 0 ]; then
		echo -en "$proc_verd""Instaŀlant Berkeley DB...\n$Color_Off"
		sudo apt-get -y install libdb-dev libdb++-dev >&3 2>&4 &
		spinner $!
	else
		echo -en "$proc_verd""Eliminant versions actuals...$Color_Off"
		if $(dpkg -l | grep -q libdb-dev); then
			sudo apt-get -y purge libdb-dev libdb++-dev >&3 2>&4 &
			spinner $!
			echo -en "\n$proc_verd""Neteja de restes...$Color_Off"
			sudo apt-get -y autoremove >&3 2>&4 &
			spinner $!
		fi
		echo -en "\n$proc_verd""Baixant Berkeley DB 4.8.30...$Color_Off"
		if [[ ! -f "db-4.8.30.NC.tar.gz" ]]; then
			wget -q http://download.oracle.com/berkeley-db/db-4.8.30.NC.tar.gz >&3 2>&4 &
			spinner $!
			if [ "$?" != 0 ]; then
				echo -e "\n$Red No s'ha pogut baixar la llibreria. Fi de l'aplicació.$Color_Off"
				exit 1
			fi
		fi
		echo -en "\n$proc_verd""Descomprimint fitxer...$Color_Off"
		tar -xvf db-4.8.30.NC.tar.gz >&3 2>&4
		spinner $!
		if [ "$?" != 0 ]; then
			echo -e "\n$Red No s'ha pogut descomprimir la llibreria. Fi de l'aplicació.$Color_Off"
			exit 1
		fi
# fix the bug on BerkeleyDB > "$script_name.log" 2>>"$file_err_log"
		sed -i 's/__atomic_compare_exchange/__atomic_compare_exchange_db/g' db-4.8.30.NC/dbinc/atomic.h
# Configure
		cd db-4.8.30.NC/build_unix
		echo -en "\n$proc_verd""Configurant Berkeley DB 4.8.30...$Color_Off"
		mkdir -p build >&3 2>&4
		../dist/configure --disable-shared --enable-cxx --with-pic --prefix="$bdb_prefix" >&3 2>&4 &
		spinner $!
		if [ "$?" != 0 ]; then
			echo -e "$\nRed No s'ha pogut configurar. Fi de l'aplicació.$Color_Off"
			exit 1
		fi
# Compile and install BerkelyDB
		echo -en "\n$proc_verd""Compilant Berkeley DB 4.8.30...$Color_Off"
		make >&3 2>&4 &
		spinner $!
		if [ "$?" != 0 ]; then
			echo -e "\n$Red No s'ha pogut compilar. Fi de l'aplicació.$Color_Off"
			exit 1
		fi
		echo -en "\n$proc_verd""Instaŀlant Berkeley DB 4.8.30...$Color_Off"
		sudo make install >&3 2>&4 &
		spinner $!
		if [ "$?" != 0 ]; then
			echo -e "\n$Red No s'ha pogut instaŀlar. Fi de l'aplicació.$Color_Off"
			exit 1
		fi
		cd ..
		sudo chown -R "$nom_moneda":"$nom_moneda" ./
	fi
	echo -e "\n$proc_verd...OK$Color_Off"
	status_coin
}
