##############################
# Usuari instaŀlador
function usuari_coin {
trap exit_handler ERR
echo -e "$Yellow-Usuari d'instaŀlació...$Color_Off"
if id "$nom_moneda" >/dev/null 2>&1; then
	if [ "$USER" != "$nom_moneda" ]; then
		sudo cp "$script_path"/"$script_name".sh /home/"$nom_moneda"/
		sudo cp "$script_path"/*.conf /home/"$nom_moneda"/
		sudo chown -R "$nom_moneda":"$nom_moneda" /home/"$nom_moneda"/*
		sudo chmod +x /home/"$nom_moneda"/*.sh
		echo -e "$proc_verd...OK$Color_Off"
		msg_usuari_fi
		read -n 1 -s -r -p ""
		sudo reboot
	fi
else
	msg_usuari
	sudo adduser --gecos "" "$nom_moneda"
	sudo usermod -aG sudo "$nom_moneda"
	sudo usermod -aG "$nom_moneda" "$USER"
	sleep 3
	sudo cp "$script_path"/"$script_name".sh /home/"$nom_moneda"/
	sudo cp "$script_path"/*.conf /home/"$nom_moneda"/
	if [ ! -f /home/"$nom_moneda"/.profile ]; then
		touch /home/"$nom_moneda"/.profile
	fi
	sudo chmod o+w /home/"$nom_moneda"/.profile
	sudo echo -e "\n./$script_name.sh\n" >> /home/"$nom_moneda"/.profile
	sudo chmod o-w /home/"$nom_moneda"/.profile
	sudo chown -R "$nom_moneda":"$nom_moneda" /home/"$nom_moneda"/*
	sudo chmod +x /home/"$nom_moneda"/*.sh
	echo -e "$proc_verd...OK$Color_Off"
	msg_usuari_fi
	read -n 1 -s -r -p ""
  echo -e $Color_Off
	sudo reboot
fi
echo -e "$proc_verd...OK$Color_Off"
}