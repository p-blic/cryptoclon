##############################
# Parell de claus
function parell_claus() {
  echo -e "$Yellow-Parell de claus$Color_Off"
	echo -e "$Green       Generant claus...$Color_Off"
  data_actual=$(date +"%Y%m%d-%H%M%S")
  openssl ecparam -genkey -name secp256k1 -out secp256k1.priv
	openssl ec -in secp256k1.priv -pubout -out secp256k1.pub
	privkey=$(openssl ec -in secp256k1.priv -outform DER|tail -c +8|head -c 32|xxd -p -c 32)
	pubkey=$(openssl ec -in secp256k1.priv -pubout -outform DER|tail -c 65|xxd -p -c 65)
	echo Clau privada: "$privkey" > "$data_actual".keys
	echo Clau pública: "$pubkey" >> "$data_actual".keys
	rm -f secp256k1.priv
	rm -f secp256k1.pub
	echo -e "$Green       El fitxer $data_actual.keys conté les claus generades$Color_Off"
}