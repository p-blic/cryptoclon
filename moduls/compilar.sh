##############################
# Compilar el codi font
function compilar() {
	trap exit_handler ERR
	cd "$git_path"
	msg_compilar
	if [ "$choice" -ne 6 ]; then
		echo -en "$Green       $nom_moneda -> Preparació...$Color_Off"
		./autogen.sh >&3 2>&4 &
		spinner $!
		echo -e "\n$Green       ...OK"
		echo -en "$Green       $nom_moneda -> Configuració...$Color_Off"
		if [ "$bdb_48" -eq 0 ]; then
			./configure --with-incompatible-bdb --disable-tests --disable-bench --disable-gui-tests >&3 2>&4 &
		else
			./configure CPPFLAGS="-I${bdb_prefix}/include/ -O2" LDFLAGS="-L${bdb_prefix}/lib/" >&3 2>&4 &
		fi
		spinner $!
		echo -e "\n$Green       ...OK"
	fi
	if [[ -f Makefile ]]; then
		echo -en "$Green       $nom_moneda -> Compilació...$Color_Off"
		make "-j"$(nproc --all) >&3 2>&4 &
		spinner $!
		echo -e "\n$Green       ...OK"
	fi
	status_coin
}

