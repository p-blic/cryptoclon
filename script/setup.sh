#!/usr/bin/env bash

# https://gitlab.com/p-blic/cryptoclon/-/raw/master/doc/setup.sh
OLD_IFS="$IFS"
IFS=';'
read updates security_updates < <(/usr/lib/update-notifier/apt-check 2>&1)
if [ $updates -gt 0 ] || [ $security_updates -gt 0 ]; then
  sudo apt-get update -y && sudo apt-get upgrade -y
fi
IFS="$OLD_IFS"
if ! command -v git &> /dev/null; then
  sudo apt-get install -y mc git
fi
if ! command -v make &> /dev/null; then
  sudo apt-get install -y make
  sudo apt install -y make-guile
fi
git clone https://gitlab.com/p-blic/cryptoclon.git
cd cryptoclon
make
make install
cd ..
sudo chmod o+w .profile
sudo echo -e "\n./$script_name.sh\n" >> .profile
sudo chmod o-w .profile
##############################
# Desinstaŀlar cloud-init
if command -v cloud-init &> /dev/null; then
  echo 'datasource_list: [ None ]' | sudo -s tee /etc/cloud/cloud.cfg.d/90_dpkg.cfg
	sudo apt-get purge -y cloud-init
	sudo rm -rf /etc/cloud/; sudo rm -rf /var/lib/cloud/
fi
echo -e "\033[0;31m\n\nEs reiniciará el sistema. Torna a fer login i executa \$ ./CryptoClon.sh\n"
read -n 1 -s -r -p "Pulsa una tecla per procedir ([CTRL-C] per cancelar)."
rm -rf setup.sh
rm -rf cryptoclon/
sudo reboot

#./CryptoClon.sh
