if [ ! -f "coin.conf" ]; then
	echo -e "\033[0;31m\n\nEl fitxer <coin.conf> no existeix."
	read -n 1 -s -r -p "Pulsa una tecla per sortir"
	echo -e "\033[0m"
	exit 1
else
  rm coin-*.conf >/dev/null 2>&1
	cp coin.conf 'coin-'"$(date +"%Y%m%d_%H%M%S").conf"
fi
source "coin.conf"

####### Paràmetres de la moneda #######################################################
nom_moneda=$(echo "$nom_moneda" | tr [:upper:] [:lower:])
nom_moneda_Mm=$(echo "$nom_moneda" | sed -e "s/\b\(.\)/\u\1/g")
nom_moneda_MM=$(echo "$nom_moneda" | tr [:lower:] [:upper:])

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# mentre continua el desenvolupament, es limita
# la compilació només al bitcoin.
nom_moneda='bitcoin'
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

if [ "$nom_moneda" == "bitcoin" ]; then
	path_moneda="bitcoin"
	bdb_48='1'
	btc_branch="0.20"
else
	btc_branch="master"
fi

####### Paràmetres generals ################################################ 
script_path="$( cd "$(dirname "$0")" >/dev/null ; pwd -P )"
script_name="${0##*/}"
script_name="${script_name%.*}"
coin_path="$script_path/$path_moneda"
git_path="$coin_path/$nom_moneda"
lock_dir="$script_path"/."$nom_moneda"_lock
file_log="$script_path/$nom_moneda.log"
file_err_log="$script_path/$nom_moneda""_err.log"
bdb_prefix="$coin_path/db-4.8.30.NC/build_unix/build"
node_status=''

####### Bloc inicial (Genesis Block). Valors definits per defecte
d_pszTimestamp="Aquesta és la meva criptomoneda, i s'anomena mycoin"	#Missatge pel coinbase
d_pubkey='04d646059440c5138448085f2b2015692d8949fabc654162d207b5d80ad1f0ca6b5b3dfed72efe857765c24966c0afcee971681e06de6c3e28911b6f4d0d7a255a' #Clau pública
d_nBits_hex='0x1d00ffff'  	#Dificultat
d_nTime='1597512051'        #Segell de temps
d_genesisReward="100"	      #Recompensa
d_startNonce='1078218270'		#Nonce inicial. Normalment 0

####### Valors calculats
total_coin=$(( "$nSubsidy" * "$nSubsidyHalvingInterval_main" * 2 ))	#Quantitan màxima de monedes
if [ "$pubkey" == '' ]; then
	pubkey="$d_pubkey"
fi
if [ "$pszTimestamp" == '' ]; then
	pszTimestamp="$d_pszTimestamp"
fi
if [ "$nBits_hex" == '' ]; then
	nBits_hex="$d_nBits_hex"
fi
if [ "$nTime" == '' ]; then
	nTime="$d_nTime"
fi
if [ "$genesisReward" == '' ]; then
	genesisReward="$d_genesisReward"
fi
if [ "$startNonce" == '' ]; then
	startNonce="$d_startNonce"
fi
nBits_dec="$(printf "%d" "$nBits_hex")"

####### Prefixos de les adreces
p_addr_src=( 
			'std::vector<unsigned char>(1,0);' 
			'std::vector<unsigned char>(1,111);' 
			'std::vector<unsigned char>(1,111);' 
			"std::vector<unsigned char>(1,5);" 
			"std::vector<unsigned char>(1,196);" 
			"std::vector<unsigned char>(1,196);" 
			'"bc";' 
			'"tb";' 
			'"bcrt";' 
			)
p_addr_dst=( 
			"std::vector<unsigned char>(1,"$p2pkh_main");" 
			"std::vector<unsigned char>(1,"$p2pkh_test");" 
			"std::vector<unsigned char>(1,"$p2pkh_reg");" 
			"std::vector<unsigned char>(1,"$p2sh_main");" 
			"std::vector<unsigned char>(1,"$p2sh_test");" 
			"std::vector<unsigned char>(1,"$p2sh_reg");" 
			'"'"$b32_main"'";' 
			'"'"$b32_test"'";' 
			'"'"$b32_reg"'";' 
			)

####### Prefixos dels missatges
p_msg_src=( 
			'0xf9;' 
			'0xbe;' 
			'0xb4;' 
			'0xd9;' 
			'0x0b;' 
			'0x11;' 
			'0x09;' 
			'0x07;' 
			'0xfa;' 
			'0xbf;' 
			'0xb5;' 
			'0xda;' 
			)
p_msg_dst=( 
			"$pm0_main"';' 
			"$pm1_main"';' 
			"$pm2_main"';' 
			"$pm3_main"';' 
			"$pm0_test"';' 
			"$pm1_test"';' 
			"$pm2_test"';' 
			"$pm3_test"';' 
			"$pm0_reg"';' 
			"$pm1_reg"';' 
			"$pm2_reg"';' 
			"$pm3_reg"';' 
			)

####### Ports de comunicació
p_rpc_src=( 
			' 8332);' 
			' 18332);' 
			' 18443);' 
			)
p_rpc_dst=( 
			' '"$rpc_main"');' 
			' '"$rpc_test"');' 
			' '"$rpc_reg"');' 
			)
p_p2p_src=( 
			' 8333;' 
			' 18333;' 
			' 18444;' 
			)
p_p2p_dst=( 
			' '"$p2p_main"';' 
			' '"$p2p_test"';' 
			' '"$p2p_reg"';' 
			)

####### Consens
cons_main_src=( 
			'nSubsidyHalvingInterval = 210000' 
			'uint256S("00000000ffffffffffffffffffffffffffffffffffffffffffffffffffffffff")' 
			'uint256S("0x00000000000000000000000000000000000000000e1ab5ec9348e9f4b8eb8154")' 
			'nPowTargetSpacing = 10' 
			'nPowTargetTimespan = 14' 
			)
cons_main_dst=( 
			'nSubsidyHalvingInterval = '"${nSubsidyHalvingInterval_main}" 
			'uint256S("'"$powlimit_main"'")' 
			'uint256S("'"$nMinimumChainWork_main"'")' 
			'nPowTargetSpacing = '"$nPowTargetSpacing_main" 
			'nPowTargetTimespan = '"$nPowTargetTimespan_main" 
			)

cons_test_src=( 
			'nSubsidyHalvingInterval = 210000' 
			'uint256S("00000000ffffffffffffffffffffffffffffffffffffffffffffffffffffffff")' 
			'uint256S("0x0000000000000000000000000000000000000000000001495c1d5a01e2af8a23")' 
			'nPowTargetSpacing = 10' 
			'nPowTargetTimespan = 14' 
			)
cons_test_dst=( 
			'nSubsidyHalvingInterval = '"${nSubsidyHalvingInterval_test}" 
			'uint256S("'"$powlimit_test"'")' 
			'uint256S("'"$nMinimumChainWork_test"'")' 
			'nPowTargetSpacing = '"$nPowTargetSpacing_test" 
			'nPowTargetTimespan = '"$nPowTargetTimespan_test" 
			)

cons_reg_src=( 
			'nSubsidyHalvingInterval = 150' 
			'uint256S("7fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff")' 
			'uint256S("0x00")' 
			'nPowTargetSpacing = 10'
			'nPowTargetTimespan = 14'
			)
cons_reg_dst=( 
			'nSubsidyHalvingInterval = '"${nSubsidyHalvingInterval_reg}" 
			'uint256S("'"$powlimit_reg"'")' 
			'uint256S("'"$nMinimumChainWork_reg"'")' 
			'nPowTargetSpacing = '"$nPowTargetSpacing_reg" 
			'nPowTargetTimespan = '"$nPowTargetTimespan_reg" 
			)

cons2_src=( 'CAmount COIN = 100000000' )
cons2_dst=( 'CAmount COIN = '"${total_coin}" )
cons3_src=( 'nSubsidy = 50' )
cons3_dst=( 'nSubsidy = '"$nSubsidy" )
cons4_src=( 
			'MAX_BLOCK_WEIGHT = 4000000' 
			'COINBASE_MATURITY = 100' 
			)
cons4_dst=( 
			'MAX_BLOCK_WEIGHT = '"${max_block_weight}" 
			'COINBASE_MATURITY = '"${coinbase_maturity}" 
			)

cons_fix_main_src=( 
			'nMinerConfirmationWindow = 2016' 
			'0x0000000000000000000f2adce67e49b0b6bdeb9de8b7c3d7e93b21e7fc1e819d' 
			'm_assumed_blockchain_size = 320' 
			)
cons_fix_main_dst=( 
			'nMinerConfirmationWindow = '$(printf "%.0f" $(( ($nPowTargetTimespan_main * 86400) / ($nPowTargetSpacing_main * 60) ))) 
			'0x00' 
			'm_assumed_blockchain_size = 0' 
			)
cons_fix_test_src=( 
			'nMinerConfirmationWindow = 2016' 
			'0x000000000000056c49030c174179b52a928c870e6e8a822c75973b7970cfbd01' 
			'm_assumed_blockchain_size = 40' 
			)
cons_fix_test_dst=( 
			'nMinerConfirmationWindow = '$(printf "%.0f" $(( ($nPowTargetTimespan_test * 86400) / ($nPowTargetSpacing_test * 60) ))) 
			'0x00' 
			'm_assumed_blockchain_size = 0' 
			)
cons_fix_reg_src=( 'nMinerConfirmationWindow = 144' )
cons_fix_reg_dst=( 'nMinerConfirmationWindow = 50' )
				
####### Activar BIPs
bip_src=( 
			'BIP34Height = 227931' 
			'BIP65Height = 388381' 
			'BIP66Height = 363725' 
			'CSVHeight = 419328' 
			'SegwitHeight = 481824' 
			'MinBIP9WarningHeight = 483840' 
			'BIP34Height = 21111' 
			'BIP65Height = 581885' 
			'BIP66Height = 330776' 
			'CSVHeight = 770112' 
			'SegwitHeight = 834624' 
			'MinBIP9WarningHeight = 836640' 
			)
bip_dst=( 
			'BIP34Height = 0' 
			'BIP65Height = 0' 
			'BIP66Height = 0' 
			'CSVHeight = 0' 
			'SegwitHeight = 0' 
			'MinBIP9WarningHeight = 0' 
			'BIP34Height = 0' 
			'BIP65Height = 0' 
			'BIP66Height = 0' 
			'CSVHeight = 0' 
			'SegwitHeight = 0' 
			'MinBIP9WarningHeight = 0' 
			)

bip_hash_src=( 
			'0x00000000000002dc756eebf4f49723ed8d30cc28a5f108eb94b1ba88ac4f9c22' 
			'0x000000000000024b89b42a942fe0d9fea3bb44ab7bd1b19115dd6a759c0808b8' 
			'0x00000000dd30457c001f4095d208cc1296b0eed002427aa599874af7a432b105' 
			'0x0000000023b3a96d3484e5abb3755c413e7d41500f8e2a5c3f0dd01299cd8ef8' 
			)

####### Punts de control
pnts_src=( 
			'1585764811,' 
			'517186863,' 
			'3.305709665792344,' 
			'1585561140,' 
			'13483,' 
			'0.08523187013249722,' 
			)
pnts_dst=( 
			'0,' 
			'0,' 
			'0,' 
			'0,' 
			'0,' 
			'0,' 
			)

####### Backup seeds
bck_seeds_src=( '{{' )
bck_seeds_dst=( '//{{' )

####### Paràmetres del bloc inicial
gb_com_src=( 
			'<< 486604799 <<' 
			"The Times 03/Jan/2009 Chancellor on brink of second bailout for banks" 
			"04678afdb0fe5548271967f1a67130b7105cd6a828e03909a67962e0ea1f61deb649f6bc3f4cef38c4f35504e51ec112de5c384df7ba0b8d578a4c702b6bf11d5f" 
			)
gb_main_src=( 
			'(1231006505' 
			' 2083236893' 
			' 0x1d00ffff' 
			', 50 ' 
			'000000000019d6689c085ae165831e934ff763ae46a2a6c172b3f1b60a8ce26f' 
			'4a5e1e4baab89f3a32518a88c31bc87f618f76673e2cc77ab2127b7afdeda33b' 
			)
gb_test_src=( 
			'(1296688602' 
			' 414098458,' 
			' 0x1d00ffff' 
			', 50 ' 
			'000000000933ea01ad0ee984209779baaec3ced90fa3f408719526f8d77f4943' 
			'4a5e1e4baab89f3a32518a88c31bc87f618f76673e2cc77ab2127b7afdeda33b' 
			)
gb_regt_src=( 
			'(1296688602' 
			' 2,' 
			' 0x207fffff' 
			', 50 ' 
			'0f9188f13cb7b2c71f2a335e3a4fc328bf5beb436012afca590b1a11466e2206' 
			'4a5e1e4baab89f3a32518a88c31bc87f618f76673e2cc77ab2127b7afdeda33b' 
			)

####### Lightning Network Daemon  ###############################
lnd_path="$coin_path/lnd"

##################################################################################################
#######################
# variables gràfiques
####### Colors
Color_Off='\033[0m'       # Text Reset
# Regular Colors
Black='\033[0;30m'        # Black
Red='\033[0;31m'          # Red
Green='\033[0;32m'        # Green
Yellow='\033[0;33m'       # Yellow
Blue='\033[0;34m'         # Blue
Purple='\033[0;35m'       # Purple
Cyan='\033[0;36m'         # Cyan
White='\033[0;37m'        # White
# Bold
BBlack='\033[1;30m'       # Black
BRed='\033[1;31m'         # Red
BGreen='\033[1;32m'       # Green
BYellow='\033[1;33m'      # Yellow
BBlue='\033[1;34m'        # Blue
BPurple='\033[1;35m'      # Purple
BCyan='\033[1;36m'        # Cyan
BWhite='\033[1;37m'       # White

menu_color="$White"
menu_user="$Red"
proc_verd="\t$Green"

########################
# variables de menú
opt_user_moneda=(
			" 1) Crear node de $nom_moneda" 
			"\t1.01) Instalar dependències" 
			"\t1.02) Clonar repositori" 
			"\t1.03) Canvi d'identitat" 
			"\t1.04) Bloc inicial" 
			"\t1.05) Configurar i compilar $nom_moneda" 
			"\t1.06) Només compilar $nom_moneda" 
			"\t1.07) Fitxer configuració" 
			"\t1.08) Test de $nom_moneda" 
			"\t1.09) Instalar $nom_moneda" 
			"\t1.10) Engedada automàtica"
			" 2) Instaŀlar Lightning Network"
			" A) Test de minat (Regtest)" 
			" B) Test de minat (Mainnet)" 
			" C) Deshabilitar $nom_moneda" 
			" D) Crear parell de claus" 
			" E) Netejar" 
			" Q) Sortir" 
		)
opt_user_no_moneda=(
			" 1) Crear node de $nom_moneda" 
			"\t1.01) N/D" 
			"\t1.02) N/D" 
			"\t1.03) N/D" 
			"\t1.04) N/D" 
			"\t1.05) N/D" 
			"\t1.06) N/D" 
			"\t1.07) N/D" 
			"\t1.08) N/D" 
			"\t1.09) N/D" 
			"\t1.10) N/D" 
			" 2) N/D"
			" A) Test de minat (Regtest)" 
			" B) Test de minat (Mainnet)" 
			" C) Deshabilitar $nom_moneda"
			" D) Crear parell de claus" 
			" E) Netejar" 
			" Q) Sortir" 
		)
opt_bitcoin=( "${opt_user_moneda[@]}" )
#(
#			" 1) Crear node de $nom_moneda" 
#			"\t1.01) N/D" 
#			"\t1.02) N/D" 
#			"\t1.03) N/D" 
#			"\t1.04) N/D" 
#			"\t1.05) N/D" 
#			"\t1.06) N/D" 
#			"\t1.07) N/D" 
#			"\t1.08) N/D" 
#			"\t1.09) N/D" 
#			"\t1.10) N/D" 
#			" 2) Instaŀlar Lightning Network"
#			" A) N/D" 
#			" B) N/D" 
#			" C) Deshabilitar $nom_moneda"
#			" D) Crear parell de claus" 
#			" E) Netejar" 
#			" Q) Sortir" 
#		)