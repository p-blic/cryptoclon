######################################################################
######################################################################
## SCRIPT
##
trap exit_handler ERR
##############################
# Comprobacions inicials
#
# Iniciar fitxers de log
if [ -d "$lock_dir" ]; then
	msg_error_create_lock
	exit 1
fi
if [ -f "$file_log" ]; then
	rm "$file_log"
fi
touch "$file_log"
exec 3<> "$file_log"
if [ -f "$file_err_log" ]; then
	rm "$file_err_log"
fi
touch "$file_err_log"
exec 4<> "$file_err_log"
# Comprovar que no s'està executant cap instància de l'script
if mkdir $lock_dir; then
	trap "final" EXIT
else
	msg_error_create_lock
	exit 1
fi
# Path de l'script
if [ "$script_path" != "$HOME" ]; then
	msg_bad_path
	exit 1
fi
# Usuari administrador i nom de moneda
if [ "$admin_user" == "$nom_moneda" ]; then
	msg_bad_admin
	exit 1
fi

##############################
# Activar sudo i mantenir actiu
if id "$nom_moneda" >/dev/null 2>&1; then
	  if ! groups $nom_moneda|grep -q sudo; then
		  sudo usermod -aG sudo "$nom_moneda" >&3 2>&4
		fi
	fi
#https://gist.github.com/cowboy/3118588
sudo -v
# Keep-alive: update existing sudo time stamp if set, otherwise do nothing.
while true; do sudo -n true; sleep 60; kill -0 "$$" || exit; done 2>/dev/null &

# Desactivar node si està engegat
#if systemctl is-active --quiet "$nom_moneda""d.service"; then
#	sudo systemctl stop "$nom_moneda"d.service >&3 2>&4
#fi

##############################
# Desinstaŀlar programes no necessaris
# snap
if command -v snap &> /dev/null; then
	msg_no_necessari
	sudo rm -rf /var/cache/snapd/ >&3 2>&4
	sudo apt-get autoremove -y --purge snapd* >&3 2>&4
	rm -fr ~/snap >&3 2>&4
fi
# cloud-init
if command -v cloud-init &> /dev/null; then
	echo 'datasource_list: [ None ]' | sudo -s tee /etc/cloud/cloud.cfg.d/90_dpkg.cfg
	sudo apt-get purge -y cloud-init >&3 2>&4
	sudo rm -rf /etc/cloud/ >&3 2>&4
	sudo rm -rf /var/lib/cloud/ >&3 2>&4
	echo -en "$BRed"
	read -n 1 -s -r -p "Es reiniciará el sistema. Torna a fer login i executar l'script de nou. Pulsa una tecla per procedir ([CTRL-C] per cancelar)."
	sudo reboot
fi

##############################
# Esborrar usuari (-d)
if [ "$1" = "-d" ]; then
	if [ "$USER" != "$nom_moneda" ]; then
		trap - ERR
		delete_autostart
#		delete_link
# Desinstaŀlar criptomoneda
		c_path="/home/$nom_moneda/$path_moneda/$nom_moneda"
		if [[ -d "$c_path" ]]; then
			if [[ -f "$c_path"/src/"$nom_moneda"d ]]; then
				cd "$c_path"
				sudo make uninstall >&3 2>&4
				cd ..
			fi
		fi
		echo -e "$BRed Esborrant usuari $nom_moneda (opció -d)"
		sudo deluser "$USER" "$nom_moneda" >&3 2>&4
		sudo userdel -fr "$nom_moneda" >&3 2>&4
		read -n 1 -s -r -p "Es sortirà del sistema. Torna a fer login i executar l'script de nou. Pulsa una tecla per procedir ([CTRL-C] per cancelar)."
	  echo -e $Color_Off
		sudo reboot
	fi
fi

##############################
# Menú
while true; do
	clear
	if [[ ! -d "$coin_path" ]]; then
		mkdir "$coin_path"
	fi
	if [ "$nom_moneda" == "bitcoin" ]; then
		options=( "${opt_bitcoin[@]}" )
		status_coin
	else
		if [ "$USER" != "$nom_moneda" ]; then
			msg_inicial
			msg_editar_conf
			msg_usuari_canvi
			options=( "${opt_user_no_moneda[@]}" )
		else
			options=( "${opt_user_moneda[@]}" )
			status_coin
		fi
	fi
	echo -e "$BCyan""Seŀlecciona una opció fent servir les fletxes pujar/baixar"
	echo -e "[ENTRAR] per confirmar."
	echo -e "$Color_Off"
	trap - ERR
	select_option "${options[@]}"
	choice=$?
	trap exit_handler ERR
	case "${options[$choice]}" in
		*"N/D"*) choice=999
			;;
		*) 
			;;
	esac
	case $choice in
		0)
			usuari_coin
			msg_instalacio
			dependencies
			clonar_repositori
			if [ "$nom_moneda" != "bitcoin" ]; then
				if [[ $install_repo != *"No"* ]]; then
					identitat
					if [[ $install_identitat != *"No"* ]]; then
						genesis
					fi
				fi
			fi
			if [[ $install_genesis != *"No"* ]] && [[ $install_bdb_status != *"No"* ]]; then
				compilar
			fi
			if [[ $install_repo != *"No"* ]]; then
				fileconf
			fi
			if [[ $install_compiled != *"No"* ]]; then
				instalar
			fi
			if [[ $install_installed != *"No"* ]] && [[ -f /etc/"$nom_moneda"/"$nom_moneda".conf ]]; then
				autostart
			fi
			pausa_tecla
			;;
		1)
			dependencies
			pausa_tecla
			;;
		2)
			clonar_repositori
			pausa_tecla
			;;
		3)
			if [ "$nom_moneda" != "bitcoin" ]; then
				if [[ $install_repo != *"No"* ]]; then
					identitat
					pausa_tecla
				fi
			fi
			;;
		4)
			if [ "$nom_moneda" != "bitcoin" ]; then
				genesis
				pausa_tecla
			fi
			;;
		5)
			if [[ $install_genesis != *"No"* ]] && [[ $install_bdb_status != *"No"* ]]; then
				compilar
				pausa_tecla
			fi
			;;
		6)
			if [[ $install_genesis != *"No"* ]] && [[ $install_bdb_status != *"No"* ]]; then
				compilar
			fi
			;;
		7)
			if [[ $install_repo != *"No"* ]]; then
				fileconf
				pausa_tecla
			fi
			;;
		8)
			test_node
			;;
		9)
			if [[ $install_compiled != *"No"* ]]; then
				instalar
				pausa_tecla
			fi
			;;
		10)
			if [[ $install_installed != *"No"* ]] && [[ -f /etc/"$nom_moneda"/"$nom_moneda".conf ]]; then
				autostart
				pausa_tecla
			fi
			;;
		11) #Opció en desenvolupament
#			if [[ $daemon_file != *"No"* ]] && [[ $daemon_enabled == *"enabled"* ]] && [[ $daemon_link == *"Sí"* ]]; then
#				lnd
#			fi
			;;
		12) #Opció en desenvolupament
#			test_minat
			;;
		13) #Opció en desenvolupament
#			test_minat
			;;
		14)
			delete_autostart
			pausa_tecla
			;;
		15)
			parell_claus
			pausa_tecla
			;;
		16)
			reset_coin
			echo -en "$Yellow"
			read -n 1 -s -r -p "S'ha finalitzat la neteja. Pulsa un tecla per tornar al menú."
			;;
		17)
			exit 0
			;;
		*)
			echo invalid option $choice;;
	esac
done