# CryptoClon
## Introducció
El CryptoClon és un script que, a patir del codi de Bitcoin, crea una criptomoneda personalitzada o un node operatiu de la xarxa Bitcoin.
## Usos
Sobre tot per experimentar
## Què fa aquest script?
1. Creació d'un nou usuari del sistema amb el mateix nom de la criptomoneda. Aquest usuari serà el que procedirà amb la instaŀlació i serà el propietari dels serveis a executar.
2. Instaŀlació de les aplicacions i llibreries necessaries.
3. Clonat del codi font de Bitcoin.
4. Si és una nova criptomoneda:
	* Canvi d'identitat.
	* Aplicació dels nous paràmetres.
	* Generació del bloc inicial (genesis block).
5. Preparació i compilació del codi font.
6. Creació del fitxer de configuració del node.
7. Integració dels binaris al sistema.
8. Instaŀlació del servei d'engegada automàtica.
## Instaŀlació

#### Requeriments
* CryptoClon s'ha probat en un Ubuntu server 18.04 LTS i en un Ubuntu 19.10. Degut a les seves característiques, no hauria d'haver problema amb cap distribució de linux basada en Debian i que faci servir paquets deb.
* L'entorn gràfic no és imprescindible pel funcionament del node, però si està instaŀlat es podran executar les aplicacions qt.
* Es recomana instaŀlar el servidor **ssh** per poder accedir remotament al node.
#### Contrasenyes
Es necessiten generar varies contrasenyes pels diferents serveis i autentificacions. Hauríen de ser com a mínim de 12 caràcters de longitud, barrejant majúscules, minúscules, números i guionets. **No feu servir caràcters especials, espais ni cometes**.

* [ **A** ]: Usuari administrador. És la contrasenya que es va posar a l'instaŀlar el sistema operatiu. Té permisos de sudo.
* [ **B** ]: Usuari criptomoneda. És la contrasenya de l'usuari generat per l'script i sota el qual es realitza la instaŀlació del node.
* [ **C** ]: RPC. Contrasenya per l'accés al node mitjançant RPC. S'ha de posar al fitxer ***coin.conf***, variable ***rpc_pass***.

#### Execució
```
	----------------------- MOLT IMPORTANT! --------------------
	L'usuari que baixa i executa CryptoClon per primera vegada
	no pot tenir el mateix nom que la nova criptomoneda, ja que
	l'script crearà un nou usuari amb aquest nom.
```
1) Verificar que **git** i **make** estan instalats. Si no:
```
	$ sudo apt install git make
```

2) Després s'ha d'executar en el terminal:

```
	$ cd $HOME
	$ git clone https://gitlab.com/p-blic/cryptoclon.git
	$ cd cryptoclon
```
3.a) Per construir un node de bitcoin:
```
	$ make
```
3.b) Per crear una criptomoneda personalitzada a partir del codi de bitcoin:
```
	$ make mycoin
```
4) Finalment:
```
	$ make install
	$ cd ..
```
Si es vol crear una nova cripomoneda s'han de configurar els paràmetres per la personalització. Aixó es fa editant el fitxer ***coin.conf*** amb un editor de text (vi, nano, ...) i sustituint els valors per defecte pels desitjats.
Un cop enllestida la configuració, ja es pot posar en marxa el procès:

		$ ./CryptoClon.sh

#### Altres opcions de *make*
Per netejar la instaŀlació:
```
	$ make clean
```
Per crear i instaŀlar només l'script (sense el fitxer coin.conf):
```
	$ make noconf
```
## Personalització
Podeu trobar tota la informació sobre els paràmetres a editar en el fitxer [params.md](doc/params.md).
