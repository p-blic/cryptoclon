##############################
# Missatges
# Totes les funcions retornen amb $Color_Off
single_asterisk=$(printf "%0.s*" {1..80})
doble_asterisk=$(printf "%0.s*" {1..80})"\n"$(printf "%0.s*" {1..80})
single_guion=$(printf "%0.s━" {1..80})
doble_guion=$(printf "%0.s━" {1..80})"\n"$(printf "%0.s━" {1..80})
single_slash=$(printf "%0.s/" {1..80})
doble_slash=$(printf "%0.s/" {1..80})"\n"$(printf "%0.s/" {1..80})

##############################
# Inici
function msg_inicial() {
	echo -e "$Yellow$single_slash"
	echo -e "$BCyan\n          Assistent d'instal·lació de $Yellow$nom_moneda$BCyan: versió 1.0"
	echo -e "$BCyan      (c) 2020 YeOldeCode https://gitlab.com/p-blic/cryptoclon\n"
	echo -e "$Yellow$single_slash$Color_Off"
}

function msg_editar_conf() {
		echo -e "$Yellow$single_guion$BCyan\n"
		echo -e "Recordeu que, abans d'executar aquest script, heu d'editar el fitxer$Yellow coin.conf$BCyan"
		echo -e "i posar els paràmetres de la vostra moneda.\n"
}

function msg_instalacio() {
	echo -e "$Yellow$doble_guion"
  echo -e ""
	echo -e "$Cyan                             ATENCIÓ!"
	echo -e ""
	echo -e "           Comença la instal·lació de$BGreen $nom_moneda$Cyan"
	echo -e ""
	echo -e "           Alguns dels processos requerits poden ser una"
	echo -e "           mica llargs. Sigueu pacients, si us plau"
	echo -e ""
	echo -e "$Yellow$doble_guion"
	echo -e "$Color_Off"
}

function msg_bad_path() {
	echo -e "$Red$doble_asterisk"
	echo -e "                   ATENCIÓ! "
	echo -e ""
	echo -e "      $script_name s'ha d'instaŀlar a $HOME"
	echo -e ""
	echo -e "$Red$doble_asterisk"
	echo -e "$Color_Off"
}

function msg_error_create_lock() {
	echo -e "$Red$doble_asterisk"
	echo -e ""
	echo -e "                   ATENCIÓ! "
	echo -e ""
	echo -e "     Hi ha una altra instància de l'script en marxa."
	echo -e "     No es pot continuar executant."
	echo -e ""
	echo -e "$Red$doble_asterisk"
	echo -e "$Color_Off"
}

function msg_error_delete_lock() {
	echo -e "$Red$doble_asterisk"
	echo -e ""
	echo -e "                   ATENCIÓ! "
	echo -e ""
	echo -e "     No s'ha pogut esborrar el blocatge de l'script"
	echo -e ""
	echo -e "$Red$doble_asterisk"
	echo -e "$Color_Off"
}
##############################
# Eliminar programes no necessaris
function msg_no_necessari() {
	echo -e "$Yellow$doble_guion"
	echo -e "$Cyan                   ATENCIÓ!"
	echo -e ""
	echo -e " Es desinstal·larà software no necessari"
	echo -e ""
	echo -e "  Aquest procès només s'executa un cop."
	echo -e "  Quan acabi, es reiniciarà el sistema."
	echo -e "$Yellow$doble_guion"
	echo -e "$Color_Off"
}

##############################
# Reset
function msg_reset() {
	echo ""
	echo -e "$Red$doble_asterisk"
	echo -e "              ATENCIÓ! Avis important"
	echo -e ""
	echo -e "Es desinstaŀlarà $nom_moneda i s'esborraran les configuracions i els serveis"
	echo -e ""
	echo -e "S'esborrarà$BRed TOT EL CONTINGUT$Red del directori actual."
	echo -e ""
	echo -e "Només quedaran els fitxers de l'script, la resta serà$BRed irrecuperable$Red."
	echo -e ""
	echo -e "També s'esborrarà el directori que conté la cadena de blocs i els wallets"
	echo -e ""
	echo -e "En cas de dubte selecciona NO."
	echo -e ""
	echo -e "$Red$doble_asterisk"
	echo -e "$Color_Off"
}

##############################
# Usuari
function msg_usuari(){
	echo -e "$Yellow$single_guion"
	echo -e "$Cyan                   ATENCIÓ!"
	echo -e ""
	echo -e " Es crearà un nou usuari anomenat $Yellow$nom_moneda$Cyan"
	echo -e ""
	echo -e " Quan ho demani, introduïu la contrasenya [B]"
	echo -e "$Yellow$single_guion"
	echo -e "$Color_Off"
}

function msg_usuari_fi() {
	echo -e "$Yellow$doble_guion"
	echo -e "$Cyan                   ATENCIÓ!"
	echo -e "Es reiniciarà del sistema."
	echo -e ""
	echo -e "Entra amb l'usuari $BGreen$nom_moneda$Cyan i executa l'script de nou."
	echo -e "Polsa una tecla per procedir ([CTRL-C] per cancel·lar)."
	echo -e "$Yellow$doble_guion"
	echo -e "$Color_Off"
}

function msg_usuari_canvi() {
	echo -e "$Yellow$doble_guion"
	echo -e "$Cyan                        ATENCIÓ!"
	echo -e "L'usuari actual és $Yellow$USER$Cyan, amb tasques d'administrador.. Algunes opcions"
	echo -e "només es poden executar amb l'usuari $BGreen$nom_moneda$Cyan."
	echo -e "$Yellow$doble_guion"
	echo -e "$Color_Off"
}

function msg_usuari_missing() {
	echo -e "$Red$doble_asterisk"
	echo -e " L'usuari $Yellow$nom_moneda$Red no existeix. "
	echo -e " S'ha de fer una primera instal·lació abans de"
	echo -e " poder seleccionar les opcions secundàries"
	echo -e ""
	echo -e "Selecciona$Yellow 1) Crear node de $nom_moneda$Red per procedir"
	echo -e "Polsa [ENTER] per continuar"
	echo -e "$Red$doble_asterisk"
	echo -e "$Color_Off"
}

function msg_usuari_bdb() {
	echo -e "$Yellow$single_guion"
	echo -e "$Red                   ATENCIÓ!"
	echo -e "L'usuari actual és $Yellow$USER$Red."
	echo -e "La instaŀlació de Berkeley DB l'ha d'executar"
	echo -e "l'usuari $BGreen$nom_moneda."
	echo -e "$Yellow$single_guion"
	echo -e "$Color_Off"
}

function msg_bad_admin() {
	echo -e "$Red$doble_asterisk"
	echo -e "                     ATENCIÓ!"
	echo -e " Per raons de seguretat, aquest script no es pot compilar"
	echo -e " des de l'usuari base de la nova criptomoneda."
	echo -e " Cryptoclon s'ha de compilar des de l'usuari administrador "
	echo -e " que s'ha creat a l'instaŀlar el sistema operatiu."
	echo -e " L'aplicació s'aturarà immediatament"
	echo -e "$Red$doble_asterisk"
	echo -e "$Color_Off"
}

##############################
# Minat del bloc inicial
function msg_genesis_inici() {
	echo -e "$Yellow$doble_guion"
	echo -e "$Red ATENCIÓ! Aquest procés pot durar fins i tot algunes hores$Color_Off"
	echo -e "$Yellow$doble_guion"
	echo -e "$Color_Off"
}

function msg_genesis_fin() {
	echo -e "$Cyan$doble_asterisk"
	echo -e "  El fitxer $Yellow$nom_moneda.txt$Cyan conté els paràmetres"
	echo -e "  del bloc inicial de la teva moneda."
	echo -e ""
	echo -e "  Si s'han generat el parell de claus, s'han"
	echo -e "  guardat al fitxer $Yellow$nom_moneda.keys$Cyan"
	echo -e ""
	echo -e "  Les dades contingudes en aquests dos fitxers són"
	echo -e "  com la partida de naixement del $Yellow$nom_moneda$Cyan. Si es"
	echo -e "  perden ja no es pot tornar a generar el bloc"
	echo -e "  inicial tal con és ara. Conserva-les amb cura."
	echo -e "$Cyan$doble_asterisk"
	echo -e "$Color_Off"
}

##############################
# Compilar
function msg_compilar() {
		echo -e "$Yellow-Compilació del codi font...$Color_Off"
		echo -e "$Red$single_asterisk"
		echo -e "$Red ATENCIÓ! Aquest procés és pot allargar més d'una hora"
		echo -e "$Red$single_asterisk"
		echo -e "$Color_Off"
}

function msg_comp_fin() {
	echo -e "$Yellow$doble_guion"
	echo -e "$Cyan Compilació i instal·lació de $nom_moneda finalitzades"
	echo -e " Polsa una tecla per tornar al menú"
	echo -e "$Yellow$doble_guion"
	echo -e "$Color_Off"
}

##############################
# Instaŀlar
function msg_ins() {
  echo -e "$Yellow-Instaŀlació dels executables..."
	echo -e "$Color_Off"
}

##############################
# Test minat
function msg_test_minat_r() {
	echo -e "$Yellow$doble_guion"
	echo -e "                      Test de minat"
	echo ""
	echo -e "$Cyan Es posaran en marxa tres nodes i minaran $nom_moneda simultàniament."
	echo -e " La pantalla mostrarà la quantitat de blocs que hi ha a la cadena."
	echo -e "$Yellow$doble_guion"
	echo -e "$Color_Off"
}

##############################
# Test funcionament node
function msg_test_node() {
	echo -e "$Yellow$doble_guion"
	echo -e "$Cyan  S'engegarà un node de $nom_moneda a la cadena Regtest"
	echo -e "  Si tot va be, el node romandrà en marxa durant 90 segons i s'aturarà"
	echo -e "$Yellow$doble_guion"
	echo -e "$Color_Off"
}

function msg_test_node_fi() {
	echo -e "$Yellow$single_guion"
	echo -e "$Green Fi del test."
	echo -e " Si la darrera línia diu $Cyan""Shutdown: done$Green el node funciona correctament."
	echo -e " Pots consultar la sortida del node al fitxer$Cyan nodetest.log"
	echo -e "$Yellow$single_guion"
	echo -e "$Color_Off"
}

function msg_test_nocomp() {
	echo -e "$Red$single_guion"
	echo -e "El node no està compilat o s'ha esborrat l'executable."
	echo -e "Compila de nou i torna a executar el test."
	echo -e "$single_guion"
	echo -e "$Color_Off"
}

function msg_node_no_instalat() {
	echo -e "$Red$single_guion"
	echo -e "El node no està instaŀlat o s'ha esborrat l'executable."
	echo -e "Compila / Instaŀla de nou i torna a executar el test."
	echo -e "$single_guion"
	echo -e "$Color_Off"
}
##############################
# Fitxer de configuració
function msg_conf_exists() {
	echo -e "$Red$single_guion"
	echo -e "El fitxer $nom_moneda.conf ja existeix."
	echo -e "Aquesta operació el sobreescriurà."
	echo -e "$single_guion"
	echo -e "$Color_Off"
}

##############################
# autostart
function msg_service_exists() {
	echo -e "$Red$single_guion"
	echo -e "El fitxer $nom_moneda""d.service ja existeix."
	echo -e "Aquesta operació el sobreescriurà."
	echo -e "$single_guion"
	echo -e "$Color_Off"
}

function msg_service_fi() {
	echo -e "$Yellow$doble_guion"
	echo -e "$Cyan El servei d'engedada automàtica ja està instaŀlat."
	echo -e " El node es posarà en marxa ara mateix."
	echo -e "$Yellow$doble_guion"
	echo -e "$Color_Off"
}