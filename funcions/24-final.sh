######################################################################
# Finalització de l'instaŀlació
# Aquesta funció s'activa al sortir de l'script amb el senyal EXIT
function final() {
	trap exit_handler ERR
#  if ! systemctl is-active --quiet "$nom_moneda""d.service"; then
#	  sudo systemctl start "$nom_moneda"d.service >&3 2>&4
#  fi
	if $(pidof sleep); then
		kill $(pidof sleep) >&3 2>&4
	fi
#Esborrar grup sudo de $moneda (OPCIONAL)
#  if id "$nom_moneda" >/dev/null 2>&1; then
#	  if groups $nom_moneda|grep -q sudo; then
#  	  sudo deluser "$nom_moneda" sudo >&3 2>&4
#		fi
#	fi
#Esborrar lock_dir
	if ! rmdir $lock_dir >&3 2>&4; then
		msg_error_delete_lock
		exit 1
	fi
	exec 3>&-
	exec 4>&-
}
