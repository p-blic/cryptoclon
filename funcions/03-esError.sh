function exit_handler ()
{
		local error_code="$?"

		test $error_code == 0 && return;

		#
		# LOCAL VARIABLES:
		# ------------------------------------------------------------------
		#    
		local i=0
		local regex=''
		local mem=''

		local error_file=''
		local error_lineno=''
		local error_message='unknown'

		local lineno=''


		#
		# PRINT THE HEADER:
		# ------------------------------------------------------------------
		#
		# Color the output if it's an interactive terminal
		#test -t 1 && tput bold; tput setf 4                                 ## red bold
		echo -e "\n$BRed******* HI HA HAGUT UN ERROR! *******\n"


		#
		# GETTING LAST ERROR OCCURRED:
		# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #

		#
		# Read last file from the error log
		# ------------------------------------------------------------------
		#
		if test -f "$file_err_log"
				then
						stderr=$( tail -n 1 "$file_err_log" )
						rm "$file_err_log"
		fi

		#
		# Managing the line to extract information:
		# ------------------------------------------------------------------
		#

		if test -n "$stderr"
				then        
						# Exploding stderr on :
						mem="$IFS"
						local shrunk_stderr=$( echo "$stderr" | sed 's/\: /\:/g' )
						IFS=':'
						local stderr_parts=( $shrunk_stderr )
						IFS="$mem"

						# Storing information on the error
						error_file="${stderr_parts[0]}"
						error_lineno="${stderr_parts[1]}"
						error_message=""

						for (( i = 3; i <= ${#stderr_parts[@]}; i++ ))
								do
										error_message="$error_message "${stderr_parts[$i-1]}": "
						done

						# Removing last ':' (colon character)
						error_message="${error_message%:*}"

						# Trim
						error_message="$( echo "$error_message" | sed -e 's/^[ \t]*//' | sed -e 's/[ \t]*$//' )"
		fi

		#
		# GETTING BACKTRACE:
		# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
		_backtrace=$( backtrace 2 )


		#
		# MANAGING THE OUTPUT:
		# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #

		local lineno=""
		regex='^([a-z]{1,}) ([0-9]{1,})$'

		if [[ $error_lineno =~ $regex ]]

				# The error line was found on the log
				# (e.g. type 'ff' without quotes wherever)
				# --------------------------------------------------------------
				then
						local row="${BASH_REMATCH[1]}"
						lineno="${BASH_REMATCH[2]}"

						echo -e "$BYellow- Fitxer: ${error_file}"
						echo -e "${row^^}: ${lineno}\n"

						echo -e "- Codi d'error: ${error_code}"             
						#test -t 1 && tput setf 6                                    ## white yellow
						echo -e "- Missatge d'error: $error_message"


				else
						regex="^${error_file}\$|^${error_file}\s+|\s+${error_file}\s+|\s+${error_file}\$"
						if [[ "$_backtrace" =~ $regex ]]

								# The file was found on the log but not the error line
								# (could not reproduce this case so far)
								# ------------------------------------------------------
								then
										echo -e "$BYellow- Fitxer: $error_file"
										echo -e "- Línia: unknown\n"

										echo -e "- Codi d'error: ${error_code}"
										#test -t 1 && tput setf 6                            ## white yellow
										echo -e "- Missatge d'error: ${stderr}"

								# Neither the error line nor the error file was found on the log
								# (e.g. type 'cp ffd fdf' without quotes wherever)
								# ------------------------------------------------------
								else
										#
										# The error file is the first on backtrace list:

										# Exploding backtrace on newlines
										mem=$IFS
										IFS='
										'
										#
										# Substring: I keep only the carriage return
										# (others needed only for tabbing purpose)
										IFS=${IFS:0:1}
										local lines=( $_backtrace )

										IFS=$mem

										error_file=""

										if test -n "${lines[1]}"
												then
														array=( ${lines[1]} )

														for (( i=2; i<${#array[@]}; i++ ))
																do
																		error_file="$error_file ${array[$i]}"
														done

														# Trim
														error_file="$( echo "$error_file" | sed -e 's/^[ \t]*//' | sed -e 's/[ \t]*$//' )"
										fi

										echo -e "$BYellow- Fitxer: $error_file"
										echo -e "- Línia: unknown\n"

										echo -e "- Codi d'error: ${error_code}"
										#test -t 1 && tput setf 6                            ## white yellow
										if test -n "${stderr}"
												then
														echo -e "- Missatge d'error: ${stderr}"
												else
														echo -e "- Missatge d'error: ${error_message}"
										fi
						fi
		fi

		#
		# PRINTING THE BACKTRACE:
		# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #

		#test -t 1 && tput setf 7                                            ## white bold
		echo -e "$_backtrace\n"

		#
		# EXITING:
		# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #

		#test -t 1 && tput setf 4                                            ## red bold
		echo -e "$BRed*************************************$Color_Off\n"

		#test -t 1 && tput sgr0 # Reset terminal

		exit "$error_code"
}

###~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~##
#
# FUNCTION: BACKTRACE
#
###~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~##

function backtrace
{
		local _start_from_=0

		local params=( "$@" )
		if (( "${#params[@]}" >= "1" ))
				then
						_start_from_="$1"
		fi

		local i=0
		local first=false
		while caller $i > /dev/null
		do
				if test -n "$_start_from_" && (( "$i" + 1   >= "$_start_from_" ))
						then
								if test "$first" == false
										then
												echo -e "$BYellow- Traçat de l'error: $BRed"
												first=true
								fi
								caller $i
				fi
				let "i=i+1"
		done
}

