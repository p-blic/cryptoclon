######################################################################
## Funció status_coin
##
function status_coin() {
	trap exit_handler ERR
	function row
	{
		trap exit_handler ERR
		local COL
		local ROW
		IFS=';' read -sdR -p $'\E[6n' ROW COL
		echo "${ROW#*[}"
	}
	old_IFS="$IFS"
# Save cursor pos
  local ROW1=$(row)
  tput sc
# Change scroll region to exclude first line
  tput csr 17 $((`tput lines` - 1))
# Move to upper-left corner
  tput cup 0 0
# Clear to the end of the line
  tput el
	
	local pad=$(printf '%0.1s' " "{1..80})
	local padlength=41
### Instalació
	local black_titol="$Black""Repo:   $Black"'no'
#Títols
	local install_bdb_status_titol="$Yellow""DB inst.: "
	local install_bdb_versio_titol="$Yellow""DB ver.:  "
	local install_repo_titol="$Yellow""Repo:     "
	local install_repo_branch_titol="$Yellow""Branca:   "
	local install_identitat_titol="$Yellow""Ident.:   "
	local install_genesis_titol="$Yellow""Génesis:  "
	local install_compiled_titol="$Yellow""Comp.:    "
	local install_installed_titol="$Yellow""Instal.:  "
#Valors
	install_bdb_status="$install_bdb_status_titol$Red"'No'		#Si/No
	install_bdb_versio="$install_bdb_versio_titol${Red}N/D"		#<X.XX.XX>
	install_repo="$install_repo_titol$Red"'No'								#Si/No
	install_repo_branch="$install_repo_branch_titol${Red}N/D"	#<branch>/----
	install_identitat="$install_identitat_titol${Red}No"			#<nom_moneda>/bitcoin
	install_genesis="$install_genesis_titol$Red""No"					#Si/No
	install_compiled="$install_compiled_titol$Red""No"				#Si/No
	install_installed="$install_installed_titol$Red""No"			#Si/No
	
### Node
#Títols
	local daemon_file_titol="$Blue""Moneda:   "
	local daemon_enabled_titol="$Blue""Estat:    "
	local daemon_status_titol="$Blue""Execució: "
	local daemon_link_titol="$Blue""Link:     "
#Valors
	daemon_file="$daemon_file_titol${Red}No           "
	daemon_enabled="$daemon_enabled_titol${Red}N/D           "
	daemon_status="$daemon_status_titol${Red}N/D           "
	daemon_link="$daemon_link_titol${Red}No           "

### LND
#Títols
	local lnd_daemon_file_titol="$Purple""LND:      "
	local lnd_daemon_enabled_titol="$Purple""Estat:    "
	local lnd_daemon_status_titol="$Purple""Exec.:    "
	local lnd_daemon_link_titol="$Purple""Link:     "
	local lnd_golang_titol="$Yellow""Golang:  "
	local lnd_go_versio_titol="$Yellow""Versió:  "
	local lnd_repo_titol="$Yellow""Repo:    "
	local lnd_repo_branch_titol="$Yellow""Branca:  "
	local lnd_compiled_titol="$Yellow""Comp.:   "
	local lnd_installed_titol="$Yellow""Instal.: "
#Valors
	lnd_daemon_file="$lnd_daemon_file_titol${Red}No           "
	lnd_daemon_enabled="$lnd_daemon_enabled_titol${Red}N/D           "
	lnd_daemon_status="$lnd_daemon_status_titol${Red}N/D           "
	lnd_daemon_link="$lnd_daemon_link_titol${Red}No           "
	lnd_golang="$lnd_golang_titol$Red"'No'
	lnd_go_versio="$lnd_go_versio_titol$Red""N/A"
	lnd_repo="$lnd_repo_titol$Red"'No'
	lnd_repo_branch="$lnd_repo_branch_titol${Red}N/D"
	lnd_compiled="$lnd_compiled_titol$Red""No"
	lnd_installed="$lnd_installed_titol$Red""No"

#----------------------------------------------------------#
### Instalació
	if [ $bdb_48 -eq 0 ]; then
		if $(dpkg -l | grep -q libdb-dev); then
			install_bdb_status="$install_bdb_status_titol""$Green""Sí"
			install_bdb_versio="$install_bdb_versio_titol""$Green"$(dpkg-query --showformat='${Version}' --show libdb-dev)
		fi
	else
		if [ -f $bdb_prefix/bin/db_load ]; then
			install_bdb_status="$install_bdb_status_titol""$Green""Sí"
			install_bdb_versio="$install_bdb_versio_titol""$Green""4.8.30"
		fi
	fi
	if [ -d "$git_path" ]; then
		cd "$git_path"
		git status >&3 2>&4
		if [ "$?" -eq 0 ]; then #git
			install_repo="$install_repo_titol$Green""clonat"
			install_repo_branch="$install_repo_branch_titol$Green"$(git rev-parse --abbrev-ref HEAD)
			cd "$git_path"/src #identitat
			if [ -f "$git_path"/src/"$nom_moneda-cli" ]; then
				install_compiled="$install_compiled_titol$Green""Sí"
			fi
			if [ -f /usr/local/bin/"$nom_moneda-cli" ]; then
				install_installed="$install_installed_titol$Green""Sí"
			fi
			if [ "$nom_moneda" == "bitcoin" ]; then
				install_identitat="$install_identitat_titol$Green$nom_moneda"
				install_genesis="$install_genesis_titol$Green""bitcoin"
			else
				if grep -Fq "$bck_seeds_dst" chainparamsseeds.h; then
					install_identitat="$install_identitat_titol$Green$nom_moneda"
					if grep -Fq "$xHash" chainparams.cpp; then
						install_genesis="$install_genesis_titol$Green""minat"
					fi
				fi
			fi
		fi
	fi
### Node
	if [[ -f /lib/systemd/system/"$nom_moneda"d.service ]]; then
		daemon_file="$daemon_file_titol$Green"'Sí'
		trap - ERR
		daemon_enabled="$daemon_enabled_titol$Green"$(systemctl is-enabled "$nom_moneda""d.service")
		trap exit_handler ERR
		daemon_status="$daemon_status_titol$Green"$(systemctl show -p ActiveState --value "$nom_moneda""d.service")"/"$(systemctl show -p SubState --value "$nom_moneda""d.service")
		if [[ -L /etc/systemd/system/"$nom_moneda""d.service" ]]; then
			daemon_link="$daemon_link_titol$Green"'Sí'
		else
			daemon_link="$daemon_link_titol$Red"'No'
		fi
	fi
#LND
	if [[ -f "/usr/local/go/bin/go" ]]; then
		lnd_golang="$lnd_golang_titol$Green""Sí"
		lnd_go_versio="$lnd_go_versio_titol$Green$go_version"
		if [[ ":$PATH:" != *":usr/local/go/bin"* ]]; then
			if [[ -f "$HOME/.profile" ]]; then
				source "$HOME/.profile"
			fi
		fi
	fi
	if [ -d "$lnd_path" ]; then
		cd "$lnd_path"
		git status >&3 2>&4
		if [ "$?" -eq 0 ]; then #git
			lnd_repo="$lnd_repo_titol$Green""clonat"
			lnd_repo_branch="$lnd_repo_branch_titol$Green$lnd_branch"
		fi
	fi	
	if [[ -f "$GOPATH/bin/lncli" ]]; then
		lnd_installed="$lnd_installed_titol$Green""Sí"
	fi
	
##########################
## Presentació
##https://stackoverflow.com/questions/4409399/padding-characters-in-printf
#
#	install_bdb_status			lnd_golang					daemon_file
#	install_bdb_versio			lnd_go_versio										daemon_enabled
#	install_repo						lnd_repo						daemon_status
#	install_repo_branch			lnd_repo_branch			daemon_link
#	install_identitat														lnd_daemon_file
#	install_genesis															lnd_daemon_enabled
#	install_compiled														lnd_daemon_status
#	install_installed				lnd_installed				lnd_daemon_link
#
	msg_inicial
	echo -e "$Yellow"
	echo -e "|━━━━━━ Coin ━━━━━━|━━━━━━━━━ LND ━━━━━━━━━━|━━━━━━━━━ DAEMONS ━━━━━━━━━━━|"
	printf '%b%*.*s%b%*.*s%b\n' " $install_bdb_status" 0 $((padlength - ${#install_bdb_status} )) "$pad" "$lnd_golang" 0 $((padlength - ${#lnd_golang} + 4 ))   "$pad" "$daemon_file"
	printf '%b%*.*s%b%*.*s%b\n' " $install_bdb_versio" 0 $((padlength - ${#install_bdb_versio} )) "$pad" "$lnd_go_versio" 0 $((padlength - ${#lnd_go_versio} + 4 )) "$pad" "$daemon_enabled"
	printf '%b%*.*s%b%*.*s%b\n' " $install_repo"       0 $((padlength - ${#install_repo} ))       "$pad" "$lnd_repo"    0 $((padlength - ${#lnd_repo} + 4 ))    "$pad" "$daemon_status"
	printf '%b%*.*s%b%*.*s%b\n' " $install_repo_branch" 0 $((padlength - ${#install_repo_branch} )) "$pad" "$lnd_repo_branch" 0 $((padlength - ${#lnd_repo_branch} + 4 )) "$pad" "$daemon_link"
	printf '%b%*.*s%b%*.*s%b\n' " $install_identitat"  0 $((padlength - ${#install_identitat} ))  "$pad" "$black_titol" 0 $((padlength - ${#black_titol} + 4 )) "$pad" "$lnd_daemon_file"
	printf '%b%*.*s%b%*.*s%b\n' " $install_genesis"    0 $((padlength - ${#install_genesis} ))    "$pad" "$black_titol" 0 $((padlength - ${#black_titol} + 4 )) "$pad" "$lnd_daemon_enabled"
	printf '%b%*.*s%b%*.*s%b\n' " $install_compiled"   0 $((padlength - ${#install_compiled} ))   "$pad" "$black_titol" 0 $((padlength - ${#black_titol} + 4 )) "$pad" "$lnd_daemon_status  "
	printf '%b%*.*s%b%*.*s%b\n' " $install_installed"  0 $((padlength - ${#install_installed} ))  "$pad" "$lnd_installed" 0 $((padlength - ${#lnd_installed} + 4 )) "$pad" "$lnd_daemon_link"
	#printf '%b%*.*s%b\n' " $daemon_file" 0 $((padlength - ${#daemon_file} )) "$pad" "$lnd_daemon_file"
	#printf '%b%*.*s%b\n' " $daemon_enabled" 0 $((padlength - ${#daemon_enabled} )) "$pad" "$lnd_daemon_enabled"
	#printf '%b%*.*s%b\n' " $daemon_status" 0 $((padlength - ${#daemon_status} )) "$pad" "$lnd_daemon_status"
	#printf '%b%*.*s%b\n' " $daemon_link" 0 $((padlength - ${#daemon_link} )) "$pad" "$lnd_daemon_link"
	#printf '%b%*.*s%b\n' "$D1" 0 $((padlength - ${#D1} )) "$pad" "$D2"
	#printf '%b%*.*s%b\n' "$D1" 0 $((padlength - ${#D1} )) "$pad" "$D2"
	echo -e "$Yellow$single_slash"

# Restore cursor position
  if [ "$ROW1" -gt 17 ]; then
    tput rc
  else
    tput cup 17 0
  fi
  IFS="$old_IFS"
}