######################################################################
## Funció my_sed
##
function my_sed() {
	sed -i -e 's|'"$src"'|'"$dst"'|g' "$file"
}

######################################################################
## Funció pausa_tecla
##
function pausa_tecla() {
	if [[ $# -eq 0 ]]; then
		msg="Polsa una tecla per continuar"
	else
		msg=$1
	fi
	echo -en "$Yellow"
	read -n 1 -s -r -p "$msg"
	echo -e "$Color_Off"
}

######################################################################
## Funció desinstaŀlar engegada automàtica
##
function delete_autostart() {
	echo -e "$Yellow-Esborrar engegada automàtica...$Color_Off"
	if [[ -f /lib/systemd/system/"$nom_moneda"d.service ]]; then
		if systemctl is-active --quiet "$nom_moneda""d.service"; then
			echo -en "$proc_verd""Desactivant node actiu...$Color_Off"
			sudo systemctl stop "$nom_moneda"d.service  >&3 2>&4 &
			spinner $!
			echo ""
		fi
		if systemctl is-enabled --quiet "$nom_moneda""d.service"; then
			echo -e "$proc_verd""Deshabilitant node..."
			sudo systemctl disable "$nom_moneda"d.service  >&3 2>&4
		fi
		echo -e "$proc_verd""Esborrant fitxer de servei..."
		sudo rm /lib/systemd/system/"$nom_moneda"d.service  >&3 2>&4
	fi
	if [[ -L /etc/systemd/system/"$nom_moneda.service" ]]; then
		echo -e "$proc_verd""Esborrant enllaç local al servei..."
	 	sudo unlink /etc/systemd/system/"$nom_moneda".service  >&3 2>&4
	fi
	echo -e "$proc_verd""...OK"
}

######################################################################
## Funció esborrar link a administrador
##
function delete_link() {
	if [[ -L /home/"$admin_user"/."$nom_moneda" ]]; then
  	sudo unlink /home/"$admin_user"/."$nom_moneda"  >&3 2>&4
	fi
}