######################################################################
## Funció clonar_repo. Clona un repositori encara que el destí a estigui ocupat
##
### https://gist.github.com/nottrobin/89bbfd5085fb31e2287858c02506325c
### https://gist.github.com/nottrobin
function clonar_repo {
  trap exit_handler ERR
	if [ -d "${destination_path}/.git" ]; then
	(
		rm -rf "$destination_path"/contrib/
  	cd ${destination_path}

		# Delete all remotes
		echo -e "$proc_verd""Esborrar remots$Color_Off"
    for remote in `git remote`; do
	  	git remote rm ${remote} >&3 2>&4
		done

    # Add origin
		echo -e "$proc_verd""Afegir remots$Color_Off"
    git remote add origin ${source_repo} >&3 2>&4
		echo -e -n "$proc_verd""Baixant dades...$Color_Off"
    git fetch origin >&3 2>&4 &
		spinner $!

    # Set default branch
		echo -e "\n$proc_verd""Definir branca per defecte$Color_Off"
    if [ -z "${branch:-}" ]; then
    	branch=`LC_ALL=C git remote show origin | grep -oP '(?<=HEAD branch: )[^ ]+$'`
      git remote set-head origin ${branch} >&3 2>&4
		else
    	git remote set-head origin -a >&3 2>&4
    fi

		# Make sure current branch is clean
		echo -e "$proc_verd""Netejar...$Color_Off"
		git clean -fd >&3 2>&4 &
		echo -e "$proc_verd""Reinicialitzar...$Color_Off"
		git reset --hard HEAD >&3 2>&4

		# Get on the desired branch
		echo -e "$proc_verd""Seŀleccionar branca per defecte...$Color_Off"
    git checkout ${branch} >&3 2>&4
		echo -e "$proc_verd""Reinicialitzar branca remota...$Color_Off"
    git reset --hard origin/${branch} >&3 2>&4

    # Delete all other branches
    branches=`git branch | grep -v \* | xargs`
    if [ -n "${branches}" ]; then
			echo -e "$proc_verd""Esborrar branques no utilitzades...$Color_Off"
    	git branch -D ${branches} >&3 2>&4
    fi
    cd ..
	)
	echo -e -n "$proc_verd""Clonant repositori...$Color_Off"
  elif [ -n "${branch:-}" ]; then
  	git clone -q -b ${branch} ${source_repo} ${destination_path} >&3 2>&4 &
		spinner $!
  else
    git clone -q ${source_repo} ${destination_path} >&3 2>&4 &
		spinner $!
  fi
	echo -e ""
	return 0
}